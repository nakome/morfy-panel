**PREVIEW:**

![Preview](https://bytebucket.org/nakome/morfy-panel/raw/12fc4db2a7712d67e6489ec223bf42c8a269f4b5/sample%20backup%20demo/preview.gif)

**INSTRUCTIONS:**

 1. Copy extra-functions.php on libraries folder

    ![copy-extra-functions.gif](https://bitbucket.org/repo/byBoMK/images/437940799-copy-extra-functions.gif)


 2. extrat admin.zip in root and go to database and open configuration.json and change  Site_url


    ![change-settings.gif](https://bitbucket.org/repo/byBoMK/images/4063338395-change-settings.gif)

 3. That's it ;)