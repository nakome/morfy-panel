<?php  defined('PANEL_ACCESS') or die('No direct script access.');


class Panel {

    const SESSION_KEY = 'notifications';

	private function __construct() {}

    private function __clone() {}

    private static $notifications = array();

    protected static $security_token_name = 'security_token_name';

    public static $version = '0.2.0';

    public static function factory(){
        return new static();
    }

    public static function partial($view){
        return include PARTIALS.DS.$view.'.php';
    }

    public static function view($view){
        return include VIEWS.DS.$view.'.php';
    }

    public static function site_url(){
        return Panel::Root(Panel::Settings('configuration','Folder cms name'));
    }

    /**
     *
     *  <code>
     *      Panel::activeLinks('g=images','class="active"');
     *      echo Panel::active('g=images','class="active"');
     *  </code>
     *
     */
    public static function activeLinks($element,$callback,$active=null){
        $url = Panel::site_url();
        $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        if(trim($actual_link) == $url.$element){
            echo $callback;
        }
        if($active == true){
            if(trim($actual_link) == $url.'/'){
                echo $callback;
            }
        }
    }
    public static function active($element,$callback,$active=null){
        $url = Panel::site_url();
        $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        if(trim($actual_link) == $url.$element){
            return $callback;
        }
        if($active == true){
            if(trim($actual_link) == $url.'/'){
                return $callback;
            }
        }
    }







    /**
     * Print the variable $data and exit if exit = true
     *
     *  <code>
     *      Panel::Debug($data);
     *  </code>
     *
     * @param mixed   $data Data
     * @param boolean $exit Exit
     */
    public static function Debug($data, $exit = false){
        echo '<pre data-codetype="Debug:">'.print_r($data, true).'</pre>';
        if ($exit) exit;
    }





    /****************************************************************
    *
    *       NOTIFICATION
    *
    *****************************************************************/

    public static function Notification_set($key, $value,$url,$reload = true){
        // template var
        $javascript =  '';
        // if true reload use location.href
        if($reload){
            $javascript .= 
                '<script>
                    document.querySelector("#wrapper").style.display = "none";
                    document.querySelector("#header").style.display = "none";

                    swal({
                        title: "'.$key.'",
                        text: "'.$value.'",
                        type: "'.$key.'",
                        timer: 2000,
                        showConfirmButton: false
                    }, function(){
                            window.location.href="'.$url.'";
                    });
                </script>';           
        }else{
            $javascript .= 
                '<script>
                    swal({
                        title: "'.$key.'",
                        text: "'.$value.'",
                        type: "'.$key.'",
                        timer: 2000,
                        showConfirmButton: false
                    }, function(){
                            window.history.back();
                    });
                </script>';
        }
        echo $javascript;
    }






    /****************************************************************
    *
    *       GENERATETOKEN
    *       PART OF MORFY CMS
    *
    *****************************************************************/

    /**
     * Generate and store a unique token which can be used to help prevent
     * [CSRF](http://wikipedia.org/wiki/Cross_Site_Request_Forgery) attacks.
     *
     *  <code>
     *      $token = Panel::generateToken();
     *  </code>
     *
     * You can insert this token into your forms as a hidden field:
     *
     *  <code>
     *      <input type="hidden" name="token" value="<?php echo Panel::generateToken(); ?>">
     *  </code>
     *
     * This provides a basic, but effective, method of preventing CSRF attacks.
     *
     * @param  boolean $new force a new token to be generated?. Default is false
     * @return string
     */
    public function generateToken($new = false){
        // Get the current token
        if (isset($_SESSION[(string) self::$security_token_name])) $token = $_SESSION[(string) self::$security_token_name]; else $token = null;
        // Create a new unique token
        if ($new === true or ! $token) {
            // Generate a new unique token
            $token = sha1(uniqid(mt_rand(), true));
            // Store the new token
            $_SESSION[(string) self::$security_token_name] = $token;
        }
        // Return token
        return $token;
    }










    /****************************************************************
    *
    *       REDIRECT SETHEADERS, SHOWDOWN
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/

    /**
     * Redirects the browser to a page specified by the $url argument.
     *
     *  <code>
     *		Panel::Redirect('test');
     *  </code>
     *
     * @param string  $url    The URL
     * @param integer $status Status
     * @param integer $delay  Delay
     */
    public static function Redirect($url, $status = 302, $delay = null){
        // Redefine vars
        $url 	= (string) $url;
        $status = (int) $status;
        // Status codes
        $messages = array();
        $messages[301] = '301 Moved Permanently';
        $messages[302] = '302 Found';
        // Is Headers sent ?
        if (headers_sent()) {
            echo "<script>document.location.href='" . $url . "';</script>\n";
        } else {
            // Redirect headers
            Panel::setHeaders('HTTP/1.1 ' . $status . ' ' . Panel::Arr_get($messages, $status, 302));
            // Delay execution
            if ($delay !== null) sleep((int) $delay);
            // Redirect
            Panel::setHeaders("Location: $url");
            // Shutdown request
            Panel::shutdown();

        }
    }
    /**
     * Set one or multiple headers.
     *
     *  <code>
     *		Panel::setHeaders('Location: http://site.com/');
     *  </code>
     *
     * @param mixed $headers String or array with headers to send.
     */
    public static function setHeaders($headers){
        // Loop elements
        foreach ((array) $headers as $header) {
            // Set header
            header((string) $header);
        }
    }
    /**
     * Terminate request
     *
     *  <code>
     *      Panel::shutdown();
     *  </code>
     *
     */
    public static function shutdown(){
        exit(0);
    }





























    /****************************************************************
    *
    *       ENVIRONMENT
    *
    *****************************************************************/



    /**
    * Set one or multiple headers.
    *
    *  <code>
    *      Panel::Environment(true);
    *  </code>
    *
    * @param integer
    */
    public static function Environment(){
        if(Panel::Settings('configuration','Debug') == "true"){
            ini_set('display_errors',1);
            error_reporting(E_ALL);
            Panel::Console('error_reporting','true');
        }else {
            ini_set('display_errors',0);
            error_reporting(E_ALL);
            Panel::Console('error_reporting','false');
        }
    }





































    /****************************************************************
    *
    *       ARR GET
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/
    /**
     * Returns value from array using "dot notation".
     * If the key does not exist in the array, the default value will be returned instead.
     *
     *  <code>
     *      $login = Arr::get($_POST, 'login');
     *
     *      $array = array('foo' => 'bar');
     *      $foo = Arr::get($array, 'foo');
     *
     *      $array = array('test' => array('foo' => 'bar'));
     *      $foo = Arr::get($array, 'test.foo');
     *  </code>
     *
     * @param  array  $array   Array to extract from
     * @param  string $path    Array path
     * @param  mixed  $default Default value
     * @return mixed
     */
    public static function Arr_get($array, $path, $default = null){
        // Get segments from path
        $segments = explode('.', $path);
        // Loop through segments
        foreach ($segments as $segment) {
            // Check
            if ( ! is_array($array) || !isset($array[$segment])) {
                return $default;
            }
            // Write
            $array = $array[$segment];
        }
        // Return
        return $array;
    }


























    /****************************************************************
    *
    *       REQUEST GET AND POST
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/

    /*
     * Get
     *
     *  <code>
     *		$action = Panel::Request_Get('action');
     *  </code>
     *
     * @param string $key Key
     * @param mixed
     */
    public static function Request_Get($key){
        return Panel::Arr_get($_GET, $key);
    }
    /**
     * Post
     *
     *  <code>
     *		$login = Panel::Request_Post('login');
     *  </code>
     *
     * @param string $key Key
     * @param mixed
     */
    public static function Request_Post($key){
        return Panel::Arr_get($_POST, $key);
    }







































    /****************************************************************
    *
    *       COOKIE SET ,GET ,DELETE
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/

    /**
     * Send a cookie
     *
     *  <code>
     *      Panel::Cookie_set('limit', 10);
     *  </code>
     *
     * @param  string  $key      A name for the cookie.
     * @param  mixed   $value    The value to be stored. Keep in mind that they will be serialized.
     * @param  integer $expire   The number of seconds that this cookie will be available.
     * @param  string  $path     The path on the server in which the cookie will be availabe. Use / for the entire domain, /foo if you just want it to be available in /foo.
     * @param  string  $domain   The domain that the cookie is available on. Use .example.com to make it available on all subdomains of example.com.
     * @param  boolean $secure   Should the cookie be transmitted over a HTTPS-connection? If true, make sure you use a secure connection, otherwise the cookie won't be set.
     * @param  boolean $httpOnly Should the cookie only be available through HTTP-protocol? If true, the cookie can't be accessed by Javascript, ...
     * @return boolean
     */
    public static function Cookie_set($key, $value, $expire = 86400, $domain = '', $path = '/', $secure = false, $httpOnly = false){
        // Redefine vars
        $key      = (string) $key;
        $value    = serialize($value);
        $expire   = time() + (int) $expire;
        $path     = (string) $path;
        $domain   = (string) $domain;
        $secure   = (bool) $secure;
        $httpOnly = (bool) $httpOnly;

        // Set cookie
        return setcookie($key, $value, $expire, $path, $domain, $secure, $httpOnly);
    }
    /**
     * Get a cookie
     *
     *  <code>
     *      $limit = Panel::Cookie_get('limit');
     *  </code>
     *
     * @param  string $key The name of the cookie that should be retrieved.
     * @return mixed
     */
    public static function Cookie_get($key){
        // Redefine key
        $key = (string) $key;

        // Cookie doesn't exist
        if( ! isset($_COOKIE[$key])) return false;

        // Fetch base value
        $value = (get_magic_quotes_gpc()) ? stripslashes($_COOKIE[$key]) : $_COOKIE[$key];

        // Unserialize
        $actual_value = @unserialize($value);

        // If unserialize failed
        if($actual_value === false && serialize(false) != $value) return false;

        // Everything is fine
        return $actual_value;

    }
    /**
     * Delete a cookie
     *
     *  <code>
     *      Panel::Cookie_delete('limit');
     *  </code>
     *
     * @param string $name The name of the cookie that should be deleted.
     */
    public static function Cookie_delete($key){
        unset($_COOKIE[$key]);
    }





















    /****************************************************************
    *
    *       SANITIZE URL
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/

    /**
     * Create safe url.
     *
     *  <code>
     *      $url = Panel::factory()->sanitizeURL($url);
     *  </code>
     *
     * @access  public
     * @param  string $url Url to sanitize
     * @return string
     */
    public function sanitizeURL($url){
        $url = trim($url);
        $url = rawurldecode($url);
        $url = str_replace(array('--','&quot;','!','@','#','$','%','^','*','(',')','+','{','}','|',':','"','<','>',
                                  '[',']','\\',';',"'",',','*','+','~','`','laquo','raquo',']>','&#8216;','&#8217;','&#8220;','&#8221;','&#8211;','&#8212;'),
                            array('-','-','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
                            $url);
        $url = str_replace('--', '-', $url);
        $url = rtrim($url, "-");
        $url = str_replace('..', '', $url);
        $url = str_replace('//', '', $url);
        $url = preg_replace('/^\//', '', $url);
        $url = preg_replace('/^\./', '', $url);

        return $url;
     }










    /****************************************************************
    *
    *       ISLOGIN,ILOGOUT
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/

    /**
     * Starts the session.
     *
     *  <code>
     *      Panel::isLogin();
     *  </code>
     *
     */
    public static function isLogin(){
        // Is session already started?
        if ($_SESSION['login']) {
            // Start the session
            return @session_start();
        }
        // If already started
        return true;
    }
    /**
     * Destroys the session.
     *
     *  <code>
     *      Panel::isLogout();
     *  </code>
     *
     */
    public static function isLogout(){
        // Destroy
        if (session_id()) {
            session_unset();
            session_destroy();
            $_SESSION = array();
        }
    }
































    /****************************************************************
    *
    *       FILE EXIST,DELETE,SCAN
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/

    /**
     * Returns true if the File exists.
     *
     *  <code>
     *      if (Panel::file_exists('filename.txt')) {
     *          // Do something...
     *      }
     *  </code>
     *
     * @param  string  $filename The file name
     * @return boolean
     */
    public static function File_exists($filename){
        // Redefine vars
        $filename = (string) $filename;
        // Return
        return (file_exists($filename) && is_file($filename));
    }
    /**
     * Delete file
     *
     *  <code>
     *      Panel::File_delete('filename.txt');
     *  </code>
     *
     * @param  mixed   $filename The file name or array of files
     * @return boolean
     */
    public static function File_delete($filename){
        // Is array
        if (is_array($filename)) {
            // Delete each file in $filename array
            foreach ($filename as $file) {
                @unlink((string) $file);
            }
        } else {
            // Is string
            return @unlink((string) $filename);
        }
    }
    /**
     * Get list of files in directory recursive
     *
     *  <code>
     *      $files = Panel::File_scan('folder','md',true);
     *  </code>
     *
     * @param  string $folder Folder
     * @param  mixed  $type   Files types
     * @return array
     */
    public static function File_scan($dir){
        $indir = array_filter(scandir($dir), function($item) {return $item;});
        return array_diff($indir, array(".", ".."));
    }





    /**
     * Rename file
     *
     *  <code>
     *      Panel::File_rename('filename1.txt', 'filename2.txt');
     *  </code>
     *
     * @param  string  $from Original file location
     * @param  string  $to   Desitination location of the file
     * @return boolean
     */
    public static function File_rename($from, $to){
        // Redefine vars
        $from = (string) $from;
        $to   = (string) $to;
        // If file exists $to than rename it
        if ( ! Panel::File_exists($to)) return rename($from, $to);
        // Else return false
        return false;
    }
    /**
     * Get the File extension.
     *
     *  <code>
     *      echo Panel::File_ext('filename.txt');
     *  </code>
     *
     * @param  string $filename The file name
     * @return string
     */
    public static function File_ext($filename){
        // Redefine vars
        $filename = (string) $filename;
        // Return file extension
        return substr(strrchr($filename, '.'), 1);
    }

    /**
     * Get the File name
     *
     *  <code>
     *      echo Panel::File_name('filename.txt');
     *  </code>
     *
     * @param  string $filename The file name
     * @return string
     */
    public static function File_name($filename){
        // Redefine vars
        $filename = (string) $filename;
        // Return filename
        return basename($filename, '.'.Panel::File_ext($filename));
    }
































    /****************************************************************
    *
    *       DIR CREATE ,EXIST,DELETE
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/


    /**
     * Creates a directory
     *
     *  <code>
     *      Panel::Dir_create('folder1');
     *  </code>
     *
     * @param  string  $dir   Name of directory to create
     * @param  integer $chmod Chmod
     * @return boolean
     */
    public static function Dir_create($dir, $chmod = 0775){
        // Redefine vars
        $dir = (string) $dir;
        // Create new dir if $dir !exists
        return ( ! Panel::Dir_exists($dir)) ? @mkdir($dir, $chmod, true) : true;
    }
    /**
     * Delete directory
     *
     *  <code>
     *      Panel::Dir_delete('folder1');
     *  </code>
     *
     * @param string $dir Name of directory to delete
     */
    public static function Dir_delete($dir){
        foreach (glob($dir."/*.*") as $filename) {
            if (is_file($filename)) {
                unlink($filename);
            }
        }
        rmdir($dir);
    }
    /**
     * Checks if this directory exists.
     *
     *  <code>
     *      if (Panel::Dir_exists('folder1')) {
     *          // Do something...
     *      }
     *  </code>
     *
     * @param  string  $dir Full path of the directory to check.
     * @return boolean
     */
    public static function Dir_exists($dir){
        // Redefine vars
        $dir = (string) $dir;
        // Directory exists
        if (file_exists($dir) && is_dir($dir)) return true;
        // Doesn't exist
        return false;
    }




    /**
     * Get list of files in directory recursive
     *
     *  <code>
     *      $files = Panel::Dir_scan('folder');
     *      $files = Panel::Dir_scan('folder', 'txt');
     *      $files = Panel::Dir_scan('folder', array('txt', 'log'));
     *  </code>
     *
     * @param  string $folder Folder
     * @param  mixed  $type   Files types
     * @return array
     */
    public static function Dir_scan($folder, $type = null){
        $data = array();
        if (is_dir($folder)) {
            $iterator = new RecursiveDirectoryIterator($folder);
            foreach (new RecursiveIteratorIterator($iterator) as $file) {
                if ($type !== null) {
                    if (is_array($type)) {
                        $file_ext = substr(strrchr($file->getFilename(), '.'), 1);
                        if (in_array($file_ext, $type)) {
                            if (strpos($file->getFilename(), $file_ext, 1)) {
                                $data[] = $file->getFilename();
                            }
                        }
                    } else {
                        if (strpos($file->getFilename(), $type, 1)) {
                            $data[] = $file->getFilename();
                        }
                    }
                } else {
                    if ($file->getFilename() !== '.' && $file->getFilename() !== '..') $data[] = $file->getFilename();
                }
            }

            return $data;
        } else {
            return false;
        }
    }















    /****************************************************************
    *
    *       GETCONTENT ,SETCONTENT
    *       PART OF MORFY CMS
    *
    *****************************************************************/


    /**
     * Fetch the content from a file or URL.
     *
     *  <code>
     *      echo Panel::getContent('filename.txt');
     *  </code>
     *
     * @param  string  $filename The file name
     * @return boolean
     */
    public static function getContent($filename){
        // Redefine vars
        $filename = (string) $filename;
        // If file exists load it
        if (Panel::File_exists($filename)) {
            return file_get_contents($filename);
        }
    }

    /**
     * Writes a string to a file.
     *
     * @param  string  $filename   The path of the file.
     * @param  string  $content    The content that should be written.
     * @param  boolean $createFile Should the file be created if it doesn't exists?
     * @param  boolean $append     Should the content be appended if the file already exists?
     * @param  integer $chmod      Mode that should be applied on the file.
     * @return boolean
     */
    public static function setContent($filename, $content, $create_file = true, $append = false, $chmod = 0666){
        // Redefine vars
        $filename    = (string) $filename;
        $content     = (string) $content;
        $create_file = (bool) $create_file;
        $append      = (bool) $append;

        // File may not be created, but it doesn't exist either
        if ( ! $create_file && Panel::File_exists($filename)) throw new RuntimeException(vsprintf("%s(): The file '{$filename}' doesn't exist", array(__METHOD__)));
        // Create directory recursively if needed
        Panel::Dir_create(dirname($filename));
        // Create file & open for writing
        $handler = ($append) ? @fopen($filename, 'a') : @fopen($filename, 'w');
        // Something went wrong
        if ($handler === false) throw new RuntimeException(vsprintf("%s(): The file '{$filename}' could not be created. Check if PHP has enough permissions.", array(__METHOD__)));
        // Store error reporting level
        $level = error_reporting();
        // Disable errors
        error_reporting(0);
        // Write to file
        $write = fwrite($handler, $content);
        // Validate write
        if($write === false) throw new RuntimeException(vsprintf("%s(): The file '{$filename}' could not be created. Check if PHP has enough permissions.", array(__METHOD__)));
        // Close the file
        fclose($handler);
        // Chmod file
        chmod($filename, $chmod);
        // Restore error reporting level
        error_reporting($level);
        // Return
        return true;
    }























    /****************************************************************
    *
    *       DATABASE FUNCTIONS
    *
    *****************************************************************/




    /*
    *   If not exist in config show out alternative
    ^   Panel:Config('config text','alternative');
    */
    public static function Config($in,$out){
        if(isset($in)) $config = $in; else $config = $out;
        return $config;
    }
    /*
    *   Get config settings
    ^   Panel::settings('password')
    */
    public static function Settings($file,$filter){
        $config = self::getContent(DATABASE.DS.$file.'.json');
        $result = json_decode($config,true);
        return $result[$filter];
    }
    /*
    *   Get database file
    ^   Panel::db('shop')
    */
    public static function db($file){
        $config = self::getContent(DATABASE.DS.$file.'.json');
        $result = json_decode($config,true);
        return $result;
    }
    /*
    *   Get language
    ^   Panel::Lang('foo')
    */
    public static function Lang($filter){
        $config = self::getContent(LANGUAGE.DS.self::Settings('configuration','language').'.json');
        $result = json_decode($config,true);
        return $result[$filter];
    }













    /****************************************************************
    *
    *       SITE URL FUNCTION
    *
    *****************************************************************/



    /*
    *   Get  Site url default panel
    */
    public static function Root($url = false){
        return self::Settings('configuration','Site_url').'/'.$url;
    }
















    /****************************************************************
    *
    *       SEO LINK
    *
    *****************************************************************/


    /*
    *   Get  pretty url like hello-world
    */
    public static function seoLink($str){
        //Lower case everything
        $str = strtolower($str);
        //Make alphanumeric (removes all other characters)
        $str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
        //Clean up multiple dashes or whitespaces
        $str = preg_replace("/[\s-]+/", " ", $str);
        //Convert whitespaces and underscore to dash
        $str = preg_replace("/[\s_]/", "-", $str);
        return $str;
    }



















    /****************************************************************
    *
    *       DEBUG PHP IN CONSOLE
    *
    *****************************************************************/

    /*
    *   Print data in javascript console
    */
    public static function Console($key,$value){
        $code = '<script>console.log("%c'.$key.' : '.$value.'", "color:#34495E; font-size:small");</script>';
        return print_r($code);
    }







    /****************************************************************
    *
    *       UPLOAD IMAGES REQUIRE RESIZE CLASS
    *
    *****************************************************************/



    /**
     * Single upload file
     *
     *  <code>
     *      Panel::uploadImages();
     *  </code>
     *
     * @access  public
     */
    public static function uploadImages(){
        // css path
        $full   = ROOTBASE.DS.'public'.DS.'images'.DS;
        // submit botton
        if(Panel::Request_post('upload')){
            // get extension
            $ext =  pathinfo($_FILES['file_upload']['name'], PATHINFO_EXTENSION);

            if(!empty($_POST['width'])) $w = $_POST['width']; else $w = '100';
            if(!empty($_POST['height'])) $h = $_POST['height']; else $h = '100';
            if(!empty($_POST['options'])) $o = $_POST['options']; else $o = 'exact';

            $n = sha1(rand(1,1000)).'.'. $ext;
            // change name if repeat
            $name = sha1($n.rand(1,100)).'.'.$ext;

            if(isset($_FILES['file_upload'])){
                try {
                    $img = new SimpleImage();
                    $img->load($_FILES['file_upload']['tmp_name']);
                    $img->resize($w,$h);
                    $img->save($full.$name);
                } catch(Exception $e) {
                    Panel::Notification_set(
                        'Error',
                        $e->getMessage(),
                        Panel::site_url().'/?g=upload_image'
                    );
                }
            }

            $photos = self::getContent(PHOTOS.DS.'photos.json');
            $result = json_decode($photos,true);
            // make uniq id
            $id = uniqid();
            $result[$id] = array(
                'id' => $id,
                'title' => htmlentities(Panel::Request_post('title')),
                'description' => htmlentities(Panel::Request_post('description')),
                'link' => htmlentities(Panel::Request_post('link')),
                'image' => $name,
                'width' => htmlentities(Panel::Request_post('width')),
                'height' => htmlentities(Panel::Request_post('height'))
            );

            // create dir if not exist
            if(!Panel::Dir_exists($full.DS.$id)) Panel::Dir_create($full.DS.$id);

            // encode
            $save = json_encode($result);
            // save content
            Panel::setContent(PHOTOS.DS.'photos.json',$save);
            // clean html and show this
            $html = '
                <span class="tools-alert tools-alert-red">'.Panel::Lang('File uploaded successfully').'.</span>
                <img class="th" style="width:80%;display:block;margin:1em auto;" src="'.Panel::Root().'/public/images/'.$name.'" alt="Image preview">
            ';

            $html .='<script src="'.Panel::site_url().'/assets/js/app.min.js"></script>

                    <script>
                        var wait = setTimeout(function(){
                            location.href = "'.Panel::site_url().'?g=images";
                        },2000);
                    </script>

              </body>
            </html>';

            die($html);
        }
    }



    /**
     * Multiple upload files
     *
     *  <code>
     *      Panel::UploadMultiple();
     *  </code>
     *
     * @access  public
     */
    public static function UploadMultiple($url,$id){
        //Loop through each file
        for($i=0; $i<count($_FILES['files']['name']); $i++) {
            //Get the temp file path
            $tmpFilePath = $_FILES['files']['tmp_name'][$i];
            //Make sure we have a filepath
            if ($tmpFilePath != ""){
                  // file
                  $file = $_FILES['files']['name'][$i];
                  // filename
                  $filename = Panel::File_name($file);
                  // extension
                  $extension = Panel::File_ext($file);
                  //Setup our new file path
                  $newFilePath = $url.DS.Panel::factory()->sanitizeURL($_FILES['files']['name'][$i]);
                  //Upload the file into the temp dir
                  if(Panel::file_exists($newFilePath)){
                    // number
                    $num = count(newFilePath);
                    $newFilePath = $url.DS.Panel::seoLink($filename).'-'.($num+1).'.'.$extension;
                  }
                  // change blank spaces for -
                  $fileUploaded = $url.DS.Panel::seoLink($filename).'.'.$extension;
                  if(move_uploaded_file($tmpFilePath, $fileUploaded)) {
                      Panel::Notification_set('success',Panel::lang('Your file has been Saved'),Panel::Site_url().'/?g=uploads');
                  }else{
                      Panel::Notification_set('error',Panel::lang('OOps and error ocurred'),Panel::Site_url().'/?g=uploads');
                  }
            }
        }
    }










    /****************************************************************
    *
    *       READ FOLDERS,FILES,ASSETS
    *
    *****************************************************************/




    /**
     * Read  folder files
     *
     *  <code>
     *      Panel::ReadFolder('../content','md');
     *  </code>
     *
     * @param  string  $folder The content folder
     * @param  string  $type File type
     * @return function
     */
    public static function ReadFolder($folder,$type=null){
        // scan folder contnet
        $entry = Panel::File_scan( $folder.'/',$type,true);
        if($entry){
            // Render folder
            panel::RenderFoldersFiles($folder.DS,$folder,$entry,$type);
        }else{
            $mode = 'markdown';
            $dir = Panel::Request_Get('g');
            echo '<span class="tools-alert tools-alert-red">'.Panel::Lang('The Folder is Empty').'  <a href="?n='.base64_encode($folder).'&t='.$mode.'&f='.$dir.'">'.Panel::lang('Add New File').'</a>  ||  <a  href="#" data-href="?dfd='.Panel::Request_Get('g').'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::lang('Are you sure to delete this folder').' \');">'.Panel::lang('Delete').' '.ucfirst(Panel::Request_Get('g')).'</a> </span>';
        }

    }



    /**
     * Read  all folders files
     *
     *  <code>
     *      Panel::ReadFolders('../content','md');
     *  </code>
     *
     * @param  string  $folder The content folder
     * @param  string  $type File type
     * @return function
     */
    public static function ReadFolders($folder,$type=null){
        // open dir ../content
        $handle = opendir($folder);
        while(($entry = readdir($handle)) !== false){
            if($entry == "." || $entry == ".."){
                continue;
            }elseif(is_dir($folder.DS.$entry)){
                $files = Panel::File_scan( $folder.'/'.$entry,$type,true);
                panel::RenderFoldersFiles($folder.'/'.$entry,$entry,$files,$type);
            }
        }
    }

    /**
     * Read  Assets files
     *
     *  <code>
     *      Panel::ReadAssetsFiles('../content','md');
     *  </code>
     *
     * @param  string  $folder The content folder
     * @param  string  $type File type css or js
     * @return function
     */
    public static function ReadAssetsFiles($folder,$type=null){

        // open dir ../content
        $handle = opendir($folder);
        while(($entry = readdir($handle)) !== false){
            if($entry == "." || $entry == ".."){
                continue;
            }elseif(is_dir($folder.DS.$entry)){
                $filetype = '';

                if($type == 'js'){
                    if(!Panel::Dir_exists($folder.'/'.$entry.'/assets/'.$type)) continue;
                    $files = Panel::File_scan($folder.'/'.$entry.'/assets/js','js',true);
                    $filetype = 'assets/js';
                }
                if($type == 'css'){
                    if(!Panel::Dir_exists($folder.'/'.$entry.'/assets/'.$type)) continue;
                    $files = Panel::File_scan( $folder.'/'.$entry.'/assets/css','css',true);
                    $filetype = 'assets/css';
                }
                if($type == 'html'){
                    $html = '';
                    $files = Panel::File_scan( $folder.'/'.$entry.'/','html',true);
                    $filetype = $html;
                }

                panel::RenderAssetsFiles($folder.'/'.$entry,$entry,$files,$type,$filetype);
            }
        }
    }









    /**
     * Show Assets files
     *
     *  <code>
     *      Panel::RenderFoldersFiles($path,$folder,$files,$type);
     *  </code>
     *
     * @param  string  $path The content folder
     * @param  string  $folder name of folder
     * @param  string  $files all content files
     * @param  string  $type  file type
     */
    public static function RenderFoldersFiles($path,$folder,$files,$type){
        $mode = 'markdown';
        $foldername = str_replace('../content/','',$folder);
        // check if request folder exist
        if(Panel::Request_Get('g')) $f = Panel::Request_Get('g'); else $f = 'main';

        $html = '';

        if($folder == '../content/'.$foldername){
             $html .= '<div class="clearfix">
                <span class="pull-left">
                    <a  href="?n='.Panel::factory()->sanitizeURL(base64_encode($path)).'&t='.$mode.'&f='.$f.'" class="btn">'.Panel::lang('New File').' '.strtoupper($type).'
                    </a>
                </span>
                <span class="pull-right">
                    <a href="#" class="btn btn-danger" onclick="return makeFolder(\'Folder Name\')">'.
                            Panel::lang('New Folder').
                        '</a>
                    <a  href="#" data-href="?dfd='.$foldername.'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::lang('Are you sure to delete this folder').' \');" class="btn btn-danger">'.Panel::lang('Delete Folder').'</a>
                </span>
            </div>
            <hr>';
        }else{
            $html = '
            <div class="clearfix">
                <span class="pull-left">
                    <a  href="?n='.Panel::factory()->sanitizeURL(base64_encode($path)).'&t='.$mode.'&f='.$f.'" class="btn">'.Panel::lang('New File').' '.strtoupper($type).'
                    </a>
                </span>
                <span class="pull-right">
                    <a href="#" class="btn btn-danger" onclick="return makeFolder(\'Folder Name\')">'.
                        Panel::lang('New Folder').
                    '</a>
                </span>
            </div>
            <hr>';
        }

        $html .= '<table class="responsive">
            <thead>
                    <tr>
                        <th class="hide-for-mobile">'.Panel::lang('Folder').'</th>
                        <th>'.Panel::lang('Files').'</th>
                        <th class="hide-for-mobile">'.Panel::lang('Type').'</th>
                        <th>'.Panel::lang('Options').'</th>
                    </tr>
            </head>
            <tbody>';
        $i = 0;
        foreach ($files as $file) {
            if(!is_Dir($path.DS.$file)){
                // remove .md
                $url = str_replace('.md','',$file);
                // remove ../content
                $folder = str_replace('../content','',$folder);
                $folder = str_replace('/','',$folder);
                $mode = 'markdown';
                // if blank return no folder
                $name = ($folder) ? $folder = $folder.'/' : $name='Root';

                    $num = $i++;

                    $url = Panel::factory()->sanitizeURL(base64_encode('../content/'.$folder.$file));

                    $get = (Panel::Request_Get('g')) ? $get = Panel::Request_Get('g') : 'main';

                    $page = str_replace('.md','',$file);
                    $html .= '<tr>
                            <td class="hide-for-mobile">'.$name.'</td>
                            <td>'.$file.'</td>
                            <td class="hide-for-mobile">'.$type.'</td>
                            <td>
                                <a  title="View" class="btn" target="_blank" href="'.Panel::Root().$folder.$page.'"><i class="ti-eye"></i></a>
                                <a  title="Edit" class="btn" href="?e='.$url.'&t='.$mode.'&f='.$get.'"><i class="ti-pencil-alt"></i></a>
                                <a  title="Rename" href="#" data-url="'.$get.'" data-root="../content/'.$folder.'" data-name="'.$page.'" data-type="'.$type.'" class="rename btn"><i class="ti-write"></i></a>
                                <a  title="Delete"  class="btn btn-danger" href="#" data-href="?g='.$get.'&dp='.$url.'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').' !\')"><i class="ti-trash"></i></a>
                            </td>
                        </tr>';
            }
        }
        $html .= '</tbody></table>';
        echo $html;
    }



    /**
     * Show Assets files
     *
     *  <code>
     *      Panel::RenderAssetsFiles($path,$folder,$files,$type);
     *  </code>
     *
     * @param  string  $path The content folder
     * @param  string  $folder name of folder
     * @param  string  $files all content files
     * @param  string  $type  file type
     */
    public static function RenderAssetsFiles($path,$folder,$files,$type,$filetype=''){

        if($type == 'css'){
            $mode = 'css';
        }elseif($type == 'js'){
            $mode = 'javascript';
        }else{
            $mode = 'php';
        }

        $html = '
        <div class="well">
        <span class="tools-alert"><b>Template: </b>'.$folder.' </span>
        <span class="pull-right"><a  class="btn" href="?n='.Panel::factory()->sanitizeURL(base64_encode($path.'/'.$filetype)).'&t='.$mode.'&f=themes">'.Panel::lang('New File').' '.strtoupper($type).'</a></span>
        <hr>

        <table class="responsive">
            <thead>
                <tr>
                    <th class="hide-for-mobile">'.Panel::lang('Folder').'</th>
                    <th>'.Panel::lang('Files').'</th>
                    <th class="hide-for-mobile">'.Panel::lang('Type').'</th>
                    <th>'.Panel::lang('Options').'</th>
                </tr>
            </thead>
            <tbody>';
        $i = 0;
        foreach ($files as $file) {
            if(!is_Dir($path.DS.$file)){
                $num = $i++;

                $get = Panel::Request_Get('g');
                $source = '';
                if($get == 'templates'){
                    $url = Panel::factory()->sanitizeURL(base64_encode($path.'/'.$filetype.$file));
                    $source = $path.'/'.$filetype;
                }else{
                    $url = Panel::factory()->sanitizeURL(base64_encode($path.'/'.$filetype.'/'.$file));
                    $source = $path.'/'.$filetype.'/';
                }

                $page = str_replace('.'.$type,'',$file);

                $html .= '
                    <tr>
                        <td class="hide-for-mobile">'.$folder.'</td>
                        <td>'.$file.'</td>
                        <td class="hide-for-mobile">'.$type.'</td>
                        <td>
                            <a  title="Edit" class="btn" href="?e='.$url.'&t='.$mode.'&f='.$get.'"><i class="ti-pencil-alt"></i></a>
                            <a title="Rename" href="#" data-url="'.$get.'" data-root="'.$source.'" data-name="'.$page.'" data-type="'.$type.'" class="rename btn"><i class="ti-write"></i></a>
                            <a  title="Delete" class="btn btn-danger" href="#" data-href="?g='.$get.'&dp='.$url.'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').' !\');"><i class="ti-trash"></i></a>
                        </td>
                    </tr>';
            }
        }
        $html .= '</tbody></table></div>';
        echo $html;
    }





    /****************************************************************
    *
    *       CREATE ZIP AND DOWNLOAD
    *       PART OF GELATO FRAMEWORK
    *
    *****************************************************************/
    /**
     * Create zip files
     *
     *  <code>
     *      Panel::Create_zip($files,$folder);
     *  </code>
     *
     * @param  array  $source_arr
     * @param  string  $destination name of folder
     */
    public static function Create_zip($source, $destination){

        @set_time_limit(0);
        @ini_set("memory_limit", "512M");

        $zippy = Zip::factory();
        foreach ($source as $s) {
            // read folder
           $zippy->readDir($s, false);
        }
        // create zip file
        $zippy->archive($destination);

    }


    /**
     * Extract zip files
     *
     *  <code>
     *      Panel::Extract_zip($files,$folder);
     *  </code>
     *
     * @param  array  $source_arr
     * @param  string  $destination name of folder
     */
    public static function Extract_zip($source, $destination){
       return Zip::factory()->extract($source,$destination);
    }




    /**
     * Forces a file to be downloaded.
     * PART OF GELATO FRAMEWORK
     *  <code>
     *      Panel::download('filename.txt');
     *  </code>
     *
     * @param string  $file         Full path to file
     * @param string  $content_type Content type of the file
     * @param string  $filename     Filename of the download
     * @param integer $kbps         Max download speed in KiB/s
     */
    public static function download($file, $content_type = null, $filename = null, $kbps = 0){
        // Redefine vars
        $file         = (string) $file;
        $content_type = ($content_type === null) ? null : (string) $content_type;
        $filename     = ($filename === null) ? null : (string) $filename;
        $kbps         = (int) $kbps;

        // Check that the file exists and that its readable
        if (file_exists($file) === false || is_readable($file) === false) {
            throw new RuntimeException(vsprintf("%s(): Failed to open stream.", array(__METHOD__)));
        }

        // Empty output buffers
        while (ob_get_level() > 0) ob_end_clean();

        // Send headers
        if ($content_type === null) $content_type = 'application/zip';

        if ($filename === null) $filename = basename($file);

        header('Content-type: ' . $content_type);
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Length: ' . filesize($file));

        // Read file and write it to the output
        @set_time_limit(0);

        if ($kbps === 0) {

            readfile($file);

        } else {

            $handle = fopen($file, 'r');

            while ( ! feof($handle) && !connection_aborted()) {

                $s = microtime(true);

                echo fread($handle, round($kbps * 1024));

                if (($wait = 1e6 - (microtime(true) - $s)) > 0) usleep($wait);

            }

            fclose($handle);
        }

        exit();
    }
}

