<?php defined('PANEL_ACCESS') or die('No direct script access.');

session_start();

/*
*	= Functions 
=============================*/
$loadFunctions = array(
	'login','sections','navigation','themes','newfile','uploads','snippets',
	'snippetsCreate','update','delete','images','images-edit','images-deleteall',
	'images-delete','settings','search','editConfigFile','editExtraFile','backup'
);
/*
*	Load functions on init
*===============================*/
foreach ($loadFunctions as $fn) {
	if(in_array($fn,$loadFunctions)){
		require_once($fn.'.php');
	}
}