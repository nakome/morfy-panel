<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('backups', function () {

    // if get create zip
    if(Panel::Request_Get('g') && Panel::Request_Get('createBackup')){
        // create zip
        $arrayOfFolders = array(
            '../content/','../public/',
            '../plugins/','../themes/',
            '../snippets/'
        );
        Panel::Create_zip($arrayOfFolders, BACKUP.DS.Panel::Request_Get('createBackup').'.zip');
         // redirect if true
        Panel::Notification_set('success',
            Panel::lang('Your file has been Saved'),Panel::Site_url().'/?g=backups');
    }

    if(Panel::Request_Get('g') && Panel::Request_Get('downloadBackup')){
        // download zip file
        $zip = Panel::Request_Get('downloadBackup');
        Panel::download($zip);
        exit;
    }
    // delete Backup
    if(Panel::Request_Get('g') && Panel::Request_Get('deleteBackup')){
        if (Panel::file_exists(Panel::Request_Get('deleteBackup'))) {
            // delete zip file
            unlink(Panel::Request_Get('deleteBackup'));
            Panel::Notification_set('success',Panel::lang('Your file has been Deteled'),Panel::Site_url().'/?g=backups');

        }
    }

    // import backup
    if(Panel::Request_post('importZip')){
        // check tokken
        if(Panel::Request_Post('token')){
          //Get the temp file path
          $tmpFilePath = $_FILES['file']['tmp_name'];
          //Make sure we have a filepath
          if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = ROOT.DS.'backup'.DS.Panel::factory()->sanitizeURL($_FILES['file']['name']);
                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                    // extract file
                    if(Panel::Extract_Zip($newFilePath,ROOTBASE.DS)){
                        // delete zip file
                        unlink($newFilePath);
                        // success
                        Panel::Notification_set('success',$image,$href.'/?g=backups');
                    }


                }else{
                    // error
                    Panel::Notification_set('success','<img src=\"assets/img/error.png\"/>',$href.'/?g=backups');
                }
            }
        }
    }


});