<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('dp', function () {
    // delete image file
    if(Panel::Request_Get('g') && Panel::Request_Get('dp')){
        $url = Panel::factory()->sanitizeURL(base64_decode(Panel::Request_Get('dp')));
        if(unlink(ROOTBASE.DS.$url)){
            if(Panel::Request_Get('g') == 'main'){
                // redirect if true
                Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url());
            }else{
                // redirect if true
                Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url().'/?g='.Panel::Request_Get('g'));                
            }
        }else{
            // error   
            Panel::Notification_set('error',Panel::lang('OOps an error ocurred..'),Panel::Site_url().'/?g='.Panel::Request_Get('g'));    
        }
    }
});

