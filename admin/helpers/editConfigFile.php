<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('EditConfigFile', function () {
    if (Panel::Request_Post('updateConfig')) {
        if(Panel::Request_Post('token')){
            if(Panel::setContent(ROOTBASE.DS.'config.php',Panel::Request_Post('content'))){
                 // redirect if true
                Panel::Notification_set('success',Panel::lang('Your file has been Update'),Panel::Site_url().'/?g=editConfigFile');   
            }else{
                Panel::Notification_set('error',Panel::lang('OOps an error ocurred..'),Panel::Site_url().'/?g=editConfigFile');  
            }
        }else{
            die('crsf detect');
        }
    }
});