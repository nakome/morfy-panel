<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('EditExtraFile', function () {
    if (Panel::Request_Post('updateConfig')) {
        if(Panel::Request_Post('token')){
            if(Panel::setContent(ROOTBASE.DS.'libraries'.DS.'extra-functions.php',Panel::Request_Post('content'))){
                Panel::Notification_set('success',Panel::lang('Your file has been Update'),Panel::Site_url().'/?g=editExtraFile');   
            }else{
                Panel::Notification_set('error',Panel::lang('OOps an error ocurred..'),Panel::Site_url().'/?g=editExtraFile');  
            }
        }else{
            die('crsf detect');
        }
    }
});