<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('deleteImage', function () {
    // delete image file
    if(Panel::Request_Get('ei') && Panel::Request_Get('dlt')){
        // id of photos
        $id = Panel::Request_Get('ei');
        $img = Panel::Request_Get('dlt');
        // delete image
        unlink(PHOTOS.DS.'images'.DS.$id.DS.$img);
        // redirect if true
        Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url().'/?g=edit_image&ei='.$id);       
    }
});