<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('deleteImages', function () {
    // delete image file
    if(Panel::Request_Get('deleteImage')){
        // id of photos
        $id = Panel::Request_Get('deleteImage');

        // remove dir
        $dir = PHOTOS.DS.'images'.DS.$id;
        foreach (glob($dir."/*.*") as $filename) {
            if (is_file($filename)) {
                unlink($filename);
            }
        }
        rmdir($dir);

        // get json 
        $photos = Panel::getContent(PHOTOS.DS.'photos.json');
        $result = json_decode($photos,true);
        // Remove image
        Panel::File_delete(PHOTOS.DS.'images'.DS.$result[$id]['image']);
        // delete id
        unset($result[$id]);
        // encode json
        $save = json_encode($result);
        // save content
        Panel::setContent(PHOTOS.DS.'photos.json',$save);  
        // redirect if true  
        Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url().'/?g=images'); 
    }
});
