<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('editImages', function () {

    if (Panel::Request_Post('editImage')) {
        if(Panel::Request_Post('token')){

            // get json 
            $photos = Panel::getContent(PHOTOS.DS.'photos.json');
            // decode
            $result = json_decode($photos,true);
            // get id
            $id = Panel::Request_Get('ei');
            // image folder
            $url = ROOTBASE.DS.'public'.DS.'images'.DS.$id;
            // filter
            $photo = $result[$id];
            
            // post title and description
            $title = (Panel::Request_Post('title')) ? Panel::Request_Post('title') : $photo['title'];
            $description = (Panel::Request_Post('description')) ? Panel::Request_Post('description') : $photo['description'];
            $link = (Panel::Request_Post('link')) ? Panel::Request_Post('link') : $photo['link'];

            // update id
            $result[$id] = array(
                'id' => $id,
                'title' => htmlentities($title),
                'description' => htmlentities($description),
                'link' => htmlentities($link),
                'image' => $photo['image'],
                'width' => $photo['width'],
                'height' => $photo['height']
            );
            // encode json
            $save = json_encode($result);
            // save content
            if(Panel::setContent(PHOTOS.DS.'photos.json',$save)){  
                Panel::UploadMultiple($url,$id);
                Panel::Notification_set('success',Panel::lang('Your file has been Update'),Panel::Site_url().'/?g=edit_image&ei='.$id); 
            }

        }else{die('crsf detect');}
    }
});
