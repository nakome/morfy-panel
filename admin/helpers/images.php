<?php defined('PANEL_ACCESS') or die('No direct script access.');


Morfy::factory()->addAction('Images', function () {
    // Check if exist folders if not  make new
    $full   = ROOTBASE.DS.'public'.DS.'images'.DS;
    // make dir  if not
    if(!Panel::Dir_exists($full)) Panel::Dir_create($full, 0755);

    // delete images
    Morfy::factory()->runAction('deleteImages');

    $photos = Panel::getContent(PHOTOS.DS.'photos.json');
    $result = json_decode($photos,true);

    if($result){

        $html = '';
        foreach ($result as $f) {
            $url = Panel::Root().'public/images/'.$f['image'];
            $html .= '
                <li class="imagePreview">
                    <a  class="imageDelete btn btn-mini btn-danger" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').'\')" href="#" data-href="?g=images&deleteImage='.$f['id'].'" ><i class="ti-trash"></i></a>
                    <a  class="imageEdit btn btn-mini" href="?g=edit_image&ei='.$f['id'].'" ><i class="ti-pencil-alt"></i></a>
                    <a class="lightCustom" href="'.$url.'">
                        <span>'.$f['title'].'</span>
                        <img  src="'.$url.'" alt="'.$f['image'].'" />
                    </a>
                </li>';
        }

        echo '<ul class="imageGallery">'.$html.'</ul>';
    }else{
        echo '<span class="tools-alert tools-alert-red">'.Panel::lang('No Images yet').'<a href="?g=upload_image">'.Panel::lang('Upload the first').' </a></span>';
    }
});