<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('Login', function () {
    // use default if empty in config.php
    $password =     Panel::Settings('configuration','Password');
    $secret_key1 =  Panel::Settings('configuration','Key_1');
    $secret_key2 =  Panel::Settings('configuration','Key_2');
    $hash = md5($secret_key1.$password.$secret_key2);

    // get actions
    if (Panel::Request_Get('action')) {
        $action = Panel::Request_Get('action');
        // swich
        switch ($action) {
            case 'login':
                // isset
                if ((Panel::Request_Post('password')) && (Panel::Request_Post('token')) && (Panel::Request_Post('password') === $password)) {
                        $_SESSION['login'] = $hash;
                        Panel::Cookie_set('login',10);
                        Panel::isLogin();
                        // redirect if true
                        Panel::Notification_set('success',Panel::lang('Hello Admin'),Panel::Site_url());   
                }else{
                    Panel::Notification_set('error',Panel::lang('You need provide a password'),Panel::Site_url()); 
                }
            break;
            case 'logout':
                Panel::Cookie_delete('login');
                Panel::isLogout();
                Panel::redirect(Panel::Root());
            break;
        }
    }
});