<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('Navigation', function () {
    foreach(glob(ROOTBASE.DS.'content'.DS.'*', GLOB_ONLYDIR) as $dir) {
        $dir = str_replace(ROOTBASE.DS.'content'.DS, '', $dir);
        echo '<li '.Panel::active('/?g='.$dir,'class="active"').'><a href="?g='.$dir.'">'.ucfirst($dir).'</a></li>';
    }
});