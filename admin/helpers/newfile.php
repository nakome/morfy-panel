<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('New_file', function () {
    $file = Panel::Request_get('n');
    $type = Panel::Request_get('t');
    $folder = Panel::Request_get('f');

    $error_info = '';

    if($type == 'css'){
        $mode = 'css';
    }elseif($type == 'javascript'){
        $mode = 'js';
    }elseif($type == 'php'){
        $mode = 'html';
    }elseif($type == 'markdown'){
        $mode = 'md';
    }

    if (Panel::Request_Post('saveFile')) {
        if(Panel::Request_Post('token')){
           // get content
            if(Panel::Request_Post('filename')) $filename = Panel::Request_Post('filename'); else $filename = '';
            if(Panel::Request_Post('content')) $content = Panel::Request_Post('content'); else $content = 'Nothing here';
            // file url
            $archive = base64_decode($file).'/'.Panel::Seolink($filename).'.'.$mode;
            // check if sexist
            if(!Panel::File_exists($archive)){
                // save file
                if(Panel::setContent($archive,$content)){
                    if(Panel::Request_Post('pages')){
                        // check if have dir like blog to reload in same dir
                        if(Panel::Request_Post('haveDir') && Panel::Request_Post('haveDir') !== 'main'){
                            Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url().'/?g='.Panel::Request_Post('haveDir'));
                        }else{
                            Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url());
                        }
                    }elseif(Panel::Request_Post('templates')){
                        Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url().'/?g='.Panel::Request_Post('templates'));
                    }elseif(Panel::Request_Post('stylesheets')){
                       Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url().'/?g='.Panel::Request_Post('stylesheets'));
                    }elseif(Panel::Request_Post('javascript')){
                       Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url().'/?g='.Panel::Request_Post('javascript'));
                    }else{
                       Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url());
                    }
                }
            }else{
                Panel::Notification_set('error',Panel::lang('File with that name already exists.'),Panel::Site_url(),false);
            }
        }else{
            die('crsf detect');
        }

    }

   return  include VIEWS.DS.'newfile.php';
});