<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('backendSearch', function () {

   $html = '';
    $pages = Morfy::factory()->getPages(ROOTBASE.DS.'content/', 'date', 'DESC', array());
    if(Panel::Request_post('submit')){
        $name = strtolower(Panel::Request_post('name'));
        $counter = 0;
        $html .= '
            <div class="row">
                <div class="box-1 col">
                    <div class="well searchResults">
                        <h3>Results for <i>'.$name.'</i></h3>';

            $html .= '<ul>';

            foreach($pages as $page){   
              $title = strtolower($page['title']);    
                if (strpos($title , $name) !== false) {
                    $url = Panel::factory()->sanitizeURL(base64_encode('../content/'.$page['url'].'.md'));
                    $html .= '<li><a href="?e='.$url.'&t=markdown"><b>Title: </b>' .$page['title']. ' - <b> Slug:</b> '.$page['slug'].'</a></li>';    
                    $counter++;
                }elseif(strpos($page['slug'], $name) !== false) {
                    $url = Panel::factory()->sanitizeURL(base64_encode('../content/'.$page['url'].'.md'));
                    $html .= '<li><a href="?e='.$url.'&t=markdown"><b>Title: </b>' .$page['title']. ' - <b> Slug:</b> '.$page['slug'].'</a></li>';    
                    $counter++;
                }
             }
            $html .= '</ul>';
            if ($counter === 0) {
                $html .= '<p><i>No matches found</i></p>';
            }else {
                $html .= '<p><b>Found: </b> ' .$counter. '</p>';
            }
            $html .= '</div></div></div>';
            echo $html;
    }
});
