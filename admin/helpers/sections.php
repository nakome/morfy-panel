<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('Sections', function () {
    // if ?g=
    if (Panel::Request_Get('g')) {
        $get = Panel::Request_Get('g');
        if($get == 'images' ||
            $get == 'edit_image' ||
            $get == 'backups' ||
            $get == 'upload_image' ||
            $get == 'uploads' ||
            $get == 'settings' ||
            $get == 'extra'||
            $get == 'templates'||
            $get == 'snippets'||
            $get == 'stylesheets'||
            $get == 'javascript' ||
            $get == 'supportchat' ||
            $get == 'editConfigFile' ||
            $get == 'editExtraFile'
        ){
            return Panel::view($get);
        }else{
            return Panel::view('other');
        }
    // create snippet if ?snp=
    }elseif(Panel::Request_Get('snp')){
        return Panel::factory()->view('snippetsCreate');
    // edit file if ?e= &t=
    }elseif(Panel::Request_Get('e') && Panel::Request_Get('t') && Panel::Request_Get('f')){
        return Panel::view('edit_file');
    // Update if ?u=
    }elseif(Panel::Request_Get('u')){
        return Morfy::factory()->runAction('Update');
    // new file if ?n & t=
    }elseif(Panel::Request_Get('n') && Panel::Request_Get('t') && Panel::Request_Get('f')){
        return Morfy::factory()->runAction('New_file');
    // new folder ?nfd=
    }elseif(Panel::Request_Get('nfd')){
        // Create folder with prompt
        Panel::Dir_create(ROOTBASE.DS.'content'.DS.Panel::Request_Get('nfd'));
        Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url().'?g='.Panel::Request_Get('nfd'));
    // delete folder ?dfd=
    }elseif(Panel::Request_Get('dfd')){
        // Create folder with prompt
        Panel::Dir_delete(ROOTBASE.DS.'content'.DS.Panel::Request_Get('dfd'));
        Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url());
    // rename  ?url &old= &new=
    }elseif(Panel::Request_Get('url') && Panel::Request_Get('old') && Panel::Request_Get('new')){
            $url = Panel::Request_Get('url');
            $oldName = Panel::Request_Get('old');
            $newName = Panel::Request_Get('new');
            if(Panel::File_rename($oldName,$newName)){
                if($url == 'main'){
                    Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url());
                }else{
                    Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'?g='.$url);
                }

            };
    }else{
        // home
        return Panel::view('main');
    }
});