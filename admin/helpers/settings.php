<?php defined('PANEL_ACCESS') or die('No direct script access.');


Morfy::factory()->addAction('settings', function () {

    // submit function
    if (Panel::Request_Post('saveConfigSettings')) {
        if(Panel::Request_Post('token')){

             // get json
            $configuration = Panel::getContent(DATABASE.DS.'configuration.json');
            // decode
            $result = json_decode($configuration,true);
            // requests
            $url = (Panel::Request_Post('siteurl')) ? Panel::Request_Post('siteurl') : $result['Site_url'];
            $timezone = (Panel::Request_Post('timezone')) ? Panel::Request_Post('timezone') : $result['Timezone'];
            $pass = (Panel::Request_Post('pass')) ? Panel::Request_Post('pass') : $result['Password'];
            $sk1 = (Panel::Request_Post('sk1')) ? Panel::Request_Post('sk1') : $result['Key_1'];
            $sk2 = (Panel::Request_Post('sk2')) ? Panel::Request_Post('sk2') : $result['Key_2'];
            $adminemail = (Panel::Request_Post('adminEmail')) ? Panel::Request_Post('adminEmail') : $result['admin_email'];
            $cmsFolder = (Panel::Request_Post('csmfolder')) ? Panel::Request_Post('csmfolder') : $result['Folder cms name'];
            $cmsName = (Panel::Request_Post('cmsName')) ? Panel::Request_Post('cmsName') : $result['Cms name'];
            $lang = (Panel::Request_Post('lang')) ? Panel::Request_Post('lang') : $result['language'];
            $Debuger = (Panel::Request_Post('Debug')) ? Panel::Request_Post('Debug') : $result['Debug'];
            // array for save
            $result = array(
                "Site_url" => $url,
                "Timezone" => $timezone,
                "Password" => $pass,
                "Key_1" => $sk1,
                "Key_2" => $sk2,
                "admin_email" => $adminemail,
                "language" => $lang,
                "Debug" => $Debuger,
                "Cms name" => $cmsName,
                "Folder cms name" => $cmsFolder,
            );
            // encode json
            $save = json_encode($result);
            // save content
            Panel::setContent(DATABASE.DS.'configuration.json',$save);
            // notification
            Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?g=settings');
        }else{die('crsf detect');}
    }
});