<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('snippets', function () {
 // delete file and folders if empty
  if(Panel::Request_get('g') && Panel::Request_get('df')){
      Panel::File_delete(ROOTBASE.DS.'snippets'.DS.Panel::Request_get('df'));
      Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url().'/?g=snippets');
  }


	// url
	$dir = ROOTBASE.DS.'snippets'.DS;
	// check if folder exist
	if(!Panel::Dir_exists($dir)) Panel::Dir_create($dir);
	$files = Panel::File_scan(ROOTBASE.DS.'snippets'.DS);
	$html = '<table class="responsive">
	     <thead>
	             <tr>
	                 <th class="hide-for-mobile">'.Panel::lang('Folder').'</th>
	                 <th>'.Panel::lang('Files').'</th>
	                 <th class="hide-for-mobile">'.Panel::lang('Type').'</th>
	                 <th>'.Panel::lang('Options').'</th>
	             </tr>
	     </head>
	     <tbody>';
	 $i = 0;
	 if($files) foreach ($files as $file) {
       $filename = Panel::File_name($file);
       $extension = Panel::File_ext($file);
       $get = (Panel::Request_Get('g')) ? $get = Panel::Request_Get('g') : 'snippets';
       if(!is_file($filename) == '.htaccess'){
	       $html .= '<tr>
	               <td class="hide-for-mobile">Snippets</td>
	               <td>'.$filename.'</td>
	               <td class="hide-for-mobile">'.$extension.'</td>
	               <td>
	                   <a  title="View" class="btn" target="_blank" href="'.Panel::Root().'snippets/'.$file.'"><i class="ti-eye"></i></a>
	                   <a  title="Edit" class="btn" href="?e='.base64_encode($dir.$filename.'.'.$extension).'&t=snippet&f=snippet"><i class="ti-pencil-alt"></i></a>
	                   <a  title="Rename" href="#" data-url="'.$get.'" data-root="../snippets/" data-name="'.$filename.'" data-type="'.$extension.'" class="rename btn"><i class="ti-write"></i></a>
	                   <a  title="Delete"  class="btn btn-danger" href="#" data-href="?g=snippets&df='.$file.'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').' !\')"><i class="ti-trash"></i></a>
	               </td>
	           </tr>';
	    }
	 }
	 $html .= '</tbody></table>';
	 echo $html;
});

