<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('createSnippets', function () {
	// url
	$dir = ROOTBASE.DS.'snippets'.DS;
	// check if folder exist
	if(!Panel::Dir_exists($dir)) Panel::Dir_create($dir);
    // create htaccess
    if(!Panel::File_exists($dir.'.htaccess')){
        $htaccess = "<FilesMatch '\.(txt|json|md|php|html|css|js)$'>
            Order allow,deny
            Deny from all
        </FilesMatch>";        
        Panel::setContent($dir.'.htaccess',$htaccess);
    };
	// save file
	if (Panel::Request_Post('saveSnippet')) {
        if(Panel::Request_Post('token')){
           // get content
            if(Panel::Request_Post('filename')) $filename = Panel::Request_Post('filename'); else $filename = '';
            if(Panel::Request_Post('content')) $content = Panel::Request_Post('content'); else $content = 'Nothing here';
            // filename
            $name = Panel::File_name($filename);
            // save file
            if(Panel::setContent($dir.Panel::seolink($name).'.php',$content)){
                Panel::Notification_set('success',Panel::lang('Your file has been Created'),Panel::Site_url().'?g=snippets'); 
            }
        }else{
            die('crsf detect');
        }
    }
});

