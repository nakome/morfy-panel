<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('Templates', function () {
    Morfy::factory()->runAction('dp');
    return Panel::ReadAssetsFiles('../themes','html');
});
Morfy::factory()->addAction('Stylesheets', function () {
    Morfy::factory()->runAction('dp');
    return Panel::ReadAssetsFiles('../themes','css');
});
Morfy::factory()->addAction('Javascript', function () {
    Morfy::factory()->runAction('dp');
    return Panel::ReadAssetsFiles('../themes','js');
});
