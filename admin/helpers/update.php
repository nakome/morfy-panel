<?php defined('PANEL_ACCESS') or die('No direct script access.');


Morfy::factory()->addAction('Update', function () {
    if (Panel::Request_Get('u')) {
        if(Panel::Request_Post('token')){
            // name of file
            $filename = Panel::Request_Get('u');
            // get content
            if(Panel::Request_Post('content')) $content = Panel::Request_Post('content'); else $content = '';
            // save
            Panel::setContent(base64_decode($filename),$content);

            if(Panel::Request_Post('pages')){
                if(Panel::Request_Post('haveDir') && Panel::Request_Post('haveDir') !== 'main'){
                    Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?g='.Panel::Request_Post('haveDir'));
                }else{
                    Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url());
                }
            }elseif(Panel::Request_Post('templates')){
                Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?g='.Panel::Request_Post('templates'));
            }elseif(Panel::Request_Post('stylesheets')){
               Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?g='.Panel::Request_Post('stylesheets'));
            }elseif(Panel::Request_Post('javascript')){
               Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?g='.Panel::Request_Post('javascript'));
            }elseif(Panel::Request_Post('snippets')){
               Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?g='.Panel::Request_Post('snippets'));
            }else{
               Panel::Notification_set('success',Panel::lang('Your file has been Updated'),Panel::Site_url().'/?e='.Panel::Request_Post('e'));
            }

        }else{
            die('crsf detect');
        }

    }
});