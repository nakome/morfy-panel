<?php defined('PANEL_ACCESS') or die('No direct script access.');

Morfy::factory()->addAction('uploadFiles', function () {

 $full = ROOTBASE.DS.'public'.DS.'uploads'.DS;
  // create dir if not exist
  if(!Panel::Dir_exists($full)) Panel::Dir_create($full);
  // Upload Files
  if(Panel::Request_post('uploadFile')){
    //Loop through each file
    for($i=0; $i<count($_FILES['filesToUpload']['name']); $i++) {
      //Get the temp file path
      $tmpFilePath = $_FILES['filesToUpload']['tmp_name'][$i];
      //Make sure we have a filepath
      if ($tmpFilePath != ""){
        // file
        $file = $_FILES['filesToUpload']['name'][$i];
        // filename
        $filename = Panel::File_name($file);
        // extension
        $extension = Panel::File_ext($file);
        // create dir if not exist
        if(!Panel::Dir_exists($full.$extension.DS)) Panel::Dir_create($full.$extension);
        //Setup our new file path
        $newFilePath = $full.$extension.DS.Panel::factory()->sanitizeURL($_FILES['filesToUpload']['name'][$i]);
        //Upload the file into the temp dir
        if(Panel::file_exists($newFilePath)){
          // number
          $num = count(newFilePath);
          $newFilePath = $full.$extension.DS.Panel::seoLink($filename).'-'.($num+1).'.'.$extension;
        }
        // change blank spaces for -
        $fileUploaded = $full.$extension.DS.Panel::seoLink($filename).'.'.$extension;
        if(move_uploaded_file($tmpFilePath, $fileUploaded)) {
            Panel::Notification_set('success',Panel::lang('Your file has been Saved'),Panel::Site_url().'/?g=uploads');
        }else{
            Panel::Notification_set('error',Panel::lang('OOps and error ocurred'),Panel::Site_url().'/?g=uploads');
        }
      }
    }

  }

  // http://stackoverflow.com/questions/1833518/remove-empty-subfolders-with-php
  function RemoveEmptySubFolders($path){
    $empty=true;
    foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file){
       $empty &= is_dir($file) && RemoveEmptySubFolders($file);
    }
    return $empty && rmdir($path);
  }
  RemoveEmptySubFolders($full);

  

  // delete file and folders if empty
  if(Panel::Request_get('g') && Panel::Request_get('df')){
      Panel::File_delete($full.Panel::Request_get('df'));
      Panel::Notification_set('success',Panel::lang('Your file has been Deleted'),Panel::Site_url().'/?g=uploads');
  }



    // get files and folders
   $files = Panel::Dir_scan( $full.'/',null);
   $html = '<table class="responsive">
         <thead>
                 <tr>
                     <th class="hide-for-mobile">'.Panel::lang('Folder').'</th>
                     <th>'.Panel::lang('Files').'</th>
                     <th class="hide-for-mobile">'.Panel::lang('Type').'</th>
                     <th>'.Panel::lang('Options').'</th>
                 </tr>
         </head>
         <tbody>';
     $i = 0;
     if($files) foreach ($files as $file) {
         if(!is_Dir($full.DS.$file)){
           $filename = Panel::File_name($file);
           $file_ext = Panel::File_ext($file);
           $get = (Panel::Request_Get('g')) ? $get = Panel::Request_Get('g') : 'uploads';
           $html .= '<tr>
                   <td class="hide-for-mobile">Uploads</td>
                   <td>'.$filename.'</td>
                   <td class="hide-for-mobile">'.$file_ext.'</td>
                   <td>
                       <a  title="View" class="btn" target="_blank" href="'.Panel::Root().'public/uploads/'.$file_ext.'/'.$file.'"><i class="ti-eye"></i></a>
                       <a  title="Rename" href="#" data-url="'.$get.'" data-root="../public/uploads/'.$file_ext.'/" data-name="'.$filename.'" data-type="'.$file_ext.'" class="rename btn"><i class="ti-write"></i></a>
                       <a  title="Delete"  class="btn btn-danger" href="#" data-href="?g=uploads&df='.$file_ext.'/'.$file.'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').' !\')"><i class="ti-trash"></i></a>
                   </td>
               </tr>';
         }
     }
     $html .= '</tbody></table>';
     echo $html;

});
