<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo Panel::site_url(); ?>/assets/fonts/themify-icons/themify-icons.css">
<link rel="stylesheet" href="<?php echo Panel::site_url(); ?>/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Panel::site_url(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo Panel::site_url(); ?>/assets/css/sweetalert.css">