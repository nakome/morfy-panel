<header id="header">
  <?php if (isset($_SESSION['login'])) {?>
     <a class="menu" href="#" title="Show Menu">
         <i class="ti-layout-sidebar-left"></i>
     </a>
   <?php } ?>

   <!-- App name -->
   <h2>
        <a href="./" class="header-title">
            <?php echo Panel::Settings('configuration','Cms name');?>
        </a>
    </h2>
   <div class="right">
       <a target="_blank" href="<?php echo Panel::Root();?>"  title="View"><i class="ti-eye"></i></a>
       <?php if (isset($_SESSION['login'])) {?>
          <a href="?g=settings" title="Settings"><i class="ti-settings"></i></a>
          <a target="_blank" href="?action=logout"  title="Logout"><i class="ti-unlock"></i></a>
        <?php } ?>
   </div>
</header>