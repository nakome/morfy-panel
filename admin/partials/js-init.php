<?php  if(Panel::Request_get('t') == 'markdown'){  ?>
<!-- library -->
<script src="<?php echo Panel::site_url().'/assets/js/library.js'; ?>"></script>
<!-- markdown -->
<script src="<?php echo Panel::site_url().'/assets/js/mte.js'; ?>"></script>
<script>
    var e = document.querySelector("#editor-area");
    var t = new MTE(document.getElementById("editor-area"), {
        tabSize: "    ",
        toolbarPosition: "before"
    });
    t.button("scissors", {
        title: "{cut}",
        click: function(e, t) {
            t.grip.insert("{cut}")
        }
    })
</script>

<?php }else{ ?>

<!-- library -->
<script src="<?php echo Panel::site_url().'/assets/js/library.js'; ?>"></script>
<!-- editor -->
<script src="<?php echo Panel::site_url().'/assets/js/hte.js'; ?>"></script>
<script>
    var txt = document.querySelector('#editor-area');
    var ns = document.querySelector('.noscript');
    if(txt && ns){
        var myEditor = new HTE(txt);
<?php if(Panel::Request_get('t') == 'php' || Panel::Request_get('snp') || Panel::Request_get('t') == 'snippet' ){ ?>
        txt.value = ns.textContent;
        ns.textContent = '';
<?php }else{ ?>
        document.querySelector('.editor-toolbar').style.display = 'none';
        txt.value = ns.textContent;
        ns.textContent = '';
<?php }; ?>
    }
</script>

<?php } ?>

<!-- Custom -->
<script src="<?php echo Panel::site_url().'/assets/js/app.min.js'; ?>"></script>
