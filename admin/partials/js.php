<script rel="javascript" src="<?php echo Panel::site_url(); ?>/assets/js/zepto.min.js"></script>
<script rel="javascript" src="<?php echo Panel::site_url(); ?>/assets/js/sweetalert.min.js"></script>
<script>
    if (navigator.appName == 'Microsoft Internet Explorer' ||  
    !!(navigator.userAgent.match(/Trident/) || 
    navigator.userAgent.match(/rv 11/)) || 
    $.browser.msie == 1){
      alert("Please dont use IE.");
      window.location.href='http://browsehappy.com/';
    }
</script>