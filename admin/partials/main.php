<?php

    defined('PANEL_ACCESS') or die('No direct script access.');

    // login function
    Morfy::factory()->runAction('Login');
    Panel::Console('Info', 'Morfy Panel\nVersion: 0.2.0\nAuthor: Moncho Varela\nUrl: http://monchovarela.es');
    
    // check if login and show sections
    if (isset($_SESSION['login'])) {
      Panel::Environment();
      Morfy::factory()->runAction('Sections');
    }else{ 
      // if not login show login form 
      Panel::Environment();
      Panel::view('login');
    } 
?>

