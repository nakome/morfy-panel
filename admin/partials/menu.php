<nav id="menu">
    <header>
        <a href="<?php echo Panel::site_url();?>">
            Menu
        </a>
    </header>
    <section>
        <ul class="navigation">
            <!-- Header -->
            <?php Panel::partial('navigation') ?>
        </ul>
    </section>
</nav>