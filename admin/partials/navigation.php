<?php  defined('PANEL_ACCESS') or die('No direct script access.'); ?>

		<?php if (isset($_SESSION['login'])) { ?>

		<!-- Pages-->
		<li class="has-dropdown">
			<a href="#"><?php echo Panel::lang('Content');?> <span class="arrow"><i class="ti-angle-right"></i></span></a>
	        <ul class="dropdown">
	        	<li><a href="<?php echo Panel::Site_url();?>"><?php echo Panel::lang('Home');?></a></li>
				<?php Morfy::factory()->runAction('Navigation');?>
	        </ul>
	    </li>

		<!-- Snippets-->
		<li class="has-dropdown">
			<a href="#"><?php echo Panel::lang('Snippets');?> <span class="arrow"><i class="ti-angle-right"></i></span></a>
			<ul class="dropdown">
				<li <?php Panel::activeLinks('/?g=snippets','class="active"'); ?>><a href="?g=snippets"><?php echo Panel::lang('View Snippets');?></a></li>
				<li <?php Panel::activeLinks('/?snp=createSnippet','class="active"'); ?>><a href="?snp=createSnippet"><?php echo Panel::lang('Create Snippet');?></a></li>
			</ul>
	    </li>	    

		<!-- Images-->
		<li class="has-dropdown">
			<a href="#"> <?php echo Panel::lang('Images');?><span class="arrow"><i class="ti-angle-right"></i></span></a>
			<ul class="dropdown">
				<li <?php Panel::activeLinks('/?g=images','class="active"'); ?>><a href="?g=images"><?php echo Panel::lang('View all Images');?></a></li>
				<li <?php Panel::activeLinks('/?g=upload_image','class="active"'); ?>><a href="?g=upload_image"><?php echo Panel::lang('Upload New File');?></a></li>
			</ul>
		</li>

		<!-- Uploads-->
		<li class="has-dropdown">
			<a href="#"> <?php echo Panel::lang('Uploads');?><span class="arrow">
				<i class="ti-angle-right"></i></span></a>
			<ul class="dropdown">
				<li <?php Panel::activeLinks('/?g=uploads','class="active"'); ?>><a href="?g=uploads"><?php echo Panel::lang('View Files');?></a></li>
			</ul>
		</li>

		<!-- Theme-->
		<li class="has-dropdown">
	  		<a href="#"> <?php echo Panel::lang('Theme');?><span class="arrow"><i class="ti-angle-right"></i></span></a>
	  		<ul class="dropdown">
				<li <?php Panel::activeLinks('/?g=templates','class="active"'); ?>><a href="?g=templates" ><?php echo Panel::lang('Templates');?></a></li>
				<li <?php Panel::activeLinks('/?g=stylesheets','class="active"'); ?>><a href="?g=stylesheets"><?php echo Panel::lang('Stylesheets');?></a></li>
				<li <?php Panel::activeLinks('/?g=javascript','class="active"'); ?>><a href="?g=javascript"><?php echo Panel::lang('Javascript');?></a></li>
			</ul>
		</li>


		<!-- Settings-->
		<li class="has-dropdown">
			<a href="#"> <?php echo Panel::lang('Settings');?><span class="arrow"><i class="ti-angle-right"></i></span></a>
			<ul class="dropdown">
				<li <?php Panel::activeLinks('/?g=settings','class="active"'); ?>><a href="?g=settings"><?php echo Panel::lang('Configuration');?></a></li>
				<li <?php Panel::activeLinks('/?g=backups','class="active"'); ?>><a href="?g=backups"><?php echo Panel::lang('Backups');?></a></li>
			</ul>
		</li>

		<!-- Support-->
		<li class="has-dropdown">
			<a href="#"> <?php echo Panel::lang('Support');?><span class="arrow"><i class="ti-angle-right"></i></span></a>
			<ul class="dropdown">
				<li><a target="_blank" href="http://morfy.org/documentation"><?php echo Panel::lang('Documentation');?></a></li>
				<li> <a target="_blank" href="http://forum.morfy.org/" title="Morfy Forum">Morfy Forum</a></li>
				<li <?php Panel::activeLinks('/?g=extra','class="active"'); ?>><a href="?g=extra">Extra</a></li>
			</ul>
		</li>

		<?php } ?>
	</ul>

	<!-- Footer -->
	<div class="footer">
			<?php Panel::partial('footer') ?>
	</div>







