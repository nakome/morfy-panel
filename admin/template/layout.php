<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>

<!DOCTYPE html>
<html>
  <head>
    <title><?php echo Panel::Settings('configuration','Cms name'); ?></title>
    <?php Panel::partial('meta') ?>
    <?php Panel::partial('css') ?>
    <?php Panel::partial('js') ?>
  </head>
  <body id="admin">
    <!-- menu -->
    <?php Panel::partial('menu') ?>
    <!-- wrapper -->
    <main id="wrapper">
        <!-- header -->
        <?php Panel::partial('header') ?>
        <!-- content -->
        <section id="content">
          <div class="container">
                <!-- Main  page -->
                <?php Panel::partial('main') ?>
          </div>
        </section>
    <!-- ================  Javascript  ================== -->
    <?php Panel::partial('js-init') ?>
  </body>
</html>