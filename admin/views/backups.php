<?php
	defined('PANEL_ACCESS') or die('No direct script access.');
	Morfy::factory()->runAction('backups');
?>

<div class="row">
	<div class="box-1 col">
		<ul class="breadcrumbs">
		  <li><a href="#"><i class="ti-home"></i></a></li>
		  <li class="unavailable"><a href="#"><?php echo Panel::lang('Settings');?></a></li>
		  <li class="current"><a href="#"><?php echo Panel::lang('Backups');?></a></li>
		</ul>
	</div>
</div>

<div id="progress">
    <div class="spinner" role="spinner">
        <div class="spinner-icon"></div>
    </div>
</div>

<div class="row">
	<div class="box-2 col">
		<span class="tools-alert tools-alert-red">
			<b><?php echo Panel::lang('Very Important');?>:</b>
			<?php echo Panel::lang('if import zip, be sure to import backup file.');?>
		</span>
	</div>

	<div class="box-2 col">
	    <form class="formFile" method="post" action="" enctype="multipart/form-data">
	    	<input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">
			<div class="pull-right">
				<input type="text" id="uploadFile" class="fake" placeholder="Choose File" disabled="disabled" />
				<div class="fileUpload btn">
					<span>Select File</span>
					<input name="file" id="importBackup" class="upload" type="file"  accept=".zip" />
				</div>
				<input type="submit" name="importZip" value="<?php echo Panel::lang('Import Backup')?>">
			</div>
		</form>
	</div>
</div>




<div class="row">
	<div class="box-1 col">
	<?php
		$files =  Morfy::factory()->getFiles('backup','zip');
		$html = '<table class="responsive">
					<thead>
						<tr>
							<th>'.Panel::lang('File').'</th>
							<th>'.Panel::lang('Date').'</th>
							<th>'.Panel::lang('Options').'</th>
						</tr>
					</thead>';

		foreach ($files as $file) {
			// convert to normal date
			$string = str_replace('backup'.DS, '', $file);
			$filename = str_replace('.zip', '', $string);
			$date = date('d/m/Y', $filename);
			$html .= '
					<tr>
						<td>'.$string.'</td>
						<td>'.$date.'</td>
						<td>
							<a class="btn" href="'.$file.'" class="button tiny"><i class="fa fa-download"></i></a>
							<a class="btn btn-danger" href="#" data-href="?g=backups&deleteBackup='.ROOT.DS.$file.'" class="button alert tiny" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').' !\')"><i class="fa fa-remove"></i></a>
						</td>
					</tr>';
		}

		$html .= '</table>';
		if($files){
			echo $html;
		}else{
			echo '<span class="tools-alert tools-alert-red">'.Panel::lang('Nothing to show.').'</span>';
		}
	?>
	</div>
</div>


<div class="row">
	<div class="box-1 col">
		<a href="<?php echo Panel::site_url();?>" class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
		<a href="<?php echo Panel::site_url();?>?g=backups&createBackup=<?php echo time(); ?>" class="btn createBackup"><?php echo Panel::Lang('Create Backup'); ?></a>
	</div>
</div>



