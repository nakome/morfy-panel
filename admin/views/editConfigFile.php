<?php
	defined('PANEL_ACCESS') or die('No direct script access.');
	Morfy::factory()->runAction('EditConfigFile');
	$config = Panel::getContent(ROOTBASE.DS.'config.php');
?>

<ul class="breadcrumbs">
  <li><a href="#"><i class="ti-home"></i></a></li>
  <li class="unavailable"><a href="#"><?php echo Panel::lang('Settings');?></a></li>
  <li class="current"><a href="#"><?php echo Panel::lang('Edit Config File');?></a></li>
</ul>

<form method="post" class="formFile">
	<input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">

	<div class="row">
		<div class="box-1 col">
			<a href="<?php echo Panel::Site_url();?>?g=settings"  class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
			<input type="submit" name="updateConfig" class="btn" value="<?php echo Panel::Lang('Update'); ?>">
		</div>
	</div>

	<div class="row">
		<div class="box-1 col">
			<div id="editor" data-type="php">
				<noscript class="noscript" class="hide"><?php echo $config; ?></noscript>
				<textarea name="content"  id="editor-area"></textarea>
			</div>
		</div>
	</div>
</form>


