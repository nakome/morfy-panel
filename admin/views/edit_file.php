<?php
	defined('PANEL_ACCESS') or die('No direct script access.');
	$edit = Panel::factory()->sanitizeURL(Panel::Request_get('e'));
    $type = Panel::factory()->sanitizeURL(Panel::Request_get('t'));
    $dir = Panel::factory()->sanitizeURL(Panel::Request_get('f'));
    $file = Panel::getContent(base64_decode($edit));
?>

<div class="row">
	<div class="box-1 col">
		<ul class="breadcrumbs">
		  <li><a href="#"><i class="ti-home"></i></a></li>
		  <li class="unavailable"><a href="#"><?php echo Panel::lang('Pages');?></a></li>
		  <li class="current"><a href="#"><?php echo Panel::lang('Edit');?></a></li>
		</ul>
	</div>
</div>

<form class="formFile" method="POST" action="<?php echo Panel::Site_url();?>/?u=<?php echo $edit; ?>">
	<input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">
	<?php if($type == 'css'){ ?>
		<input type="hidden" name="stylesheets" value="stylesheets">
	<?php }elseif($type == 'javascript'){ ?>
		<input type="hidden" name="javascript" value="javascript">
	<?php }elseif($type == 'markdown'){ ?>
		<?php if($dir){ ?>
		<input type="hidden" name="pages" value="pages">
		<input type="hidden" name="haveDir" value="<?php echo $dir;?>">
		<?php }else{ ?>
		<input type="hidden" name="pages" value="pages">
		<?php }; ?>
	<?php }elseif($type == 'php'){ ?>
		<input type="hidden" name="templates" value="templates">
	<?php }elseif($type == 'snippet'){ ?>
		<input type="hidden" name="snippets" value="snippets">
	<?php }; ?>


	<?php if($type == 'markdown'){ ?>
		<div class="row">
			<div class="box-1 col">
				<textarea name="content" class="editor-area" id="editor-area"><?php echo $file; ?></textarea>
			</div>
		</div>
	<?php }else{ ?>
		<div class="row">
			<div class="box-1 col">
				<div class="editor">
				    <div class="editor-control" id="editor-control"></div>
				    <noscript class="noscript" class="hide"><?php echo $file; ?></noscript>
				    <textarea name="content" class="editor-area" id="editor-area" placeholder="Start here.."></textarea>
				</div>
			</div>
		</div>
	<?php }; ?>


	<div class="row">
		<div class="box-1 col">
			<a href="<?php echo Panel::Site_url();?>" class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
			<input type="submit" class="btn" value="<?php echo Panel::Lang('Update'); ?>">
		</div>
	</div>
</form>