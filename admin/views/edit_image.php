<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>

<?php
	Morfy::factory()->runAction('deleteImage');
	Morfy::factory()->runAction('editImages');
    // get json
    $photos = Panel::getContent(PHOTOS.DS.'photos.json');
    // decode
    $result = json_decode($photos,true);
    // get id
    $id = Panel::Request_Get('ei');
    // filter
    $photo = $result[$id];
    // scan photos
    $dirPhotos = '';
    if(Panel::Dir_exists(PHOTOS.DS.'images'.DS.$id)){
    	$dirPhotos = Panel::File_scan(PHOTOS.DS.'images'.DS.$id);    	
    }else{
    	Panel::Dir_create(PHOTOS.DS.'images'.DS.$id);
    }

?>

	<ul class="breadcrumbs">
	  <li><a href="#"><i class="ti-home"></i></a></li>
	  <li class="unavailable"><a href="#"><?php echo Panel::lang('Images');?></a></li>
	  <li class="current"><a href="#"><?php echo Panel::lang('Edit Images');?></a></li>
	</ul>

	<div id="progress">
	    <div class="spinner" role="spinner">
	        <div class="spinner-icon"></div>
	    </div>
	</div>


	<div class="row">
		<div class="box-2 col">
			<form class="formFile" method="post" enctype="multipart/form-data">

				<input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">

				<label><?php echo Panel::lang('Images');?>:  <small>*</small>
					<input type="file"  name="files[]" id="files" multiple="multiple" accept="image/x-png, image/gif, image/jpeg" >
				</label>

				<label><?php echo Panel::lang('Title');?>:  <small>*</small>
					<input type="text"  required  name="title" id="title" value="<?php echo $photo['title'];?>">
				</label>

				<label><?php echo Panel::lang('Description');?>:  <small>*</small>
					<textarea name="description" id="description" rows="8" required><?php echo html_entity_decode($photo['description']);?></textarea>
				</label>

				<label><?php echo Panel::lang('Link');?>:  <small>*</small>
					<input type="text"  required  name="link" id="link" value="<?php echo $photo['link'];?>">
				</label>

				<a href="<?php echo Panel::Site_url();?>?g=images" class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
				<input type="submit" name="editImage" class="btn" value="<?php echo Panel::lang('Update');?>">
			</form>
		</div>
		<div  class="box-2 col">
			<pre data-codetype="Php Code:">Morfy::factory()->runAction('portfolio',array('<?php echo $photo['id'];?>',false));</pre>
			<img  style="width:100%;" src="<?php echo Panel::Root().'/public/images/'.$photo['image']; ?>" alt="">
			<?php
				if($dirPhotos){
					$html = '<ul class="mini-tumbs">';
					foreach ($dirPhotos as $image) {
					  	$html .= '<li>
					  		<a href="#" data-href="?g=edit_image&ei='.$id.'&dlt='.$image.'" onclick="confirmDelete(this.getAttribute(\'data-href\'),\' '.Panel::Lang('Are you sure').'\')">
					  			<img src="'.Panel::Root().'/public/images/'.$id.'/'.$image.'" alt="">
					  		</a>
					  	</li>';
					}
					$html .= '</ul>';
					echo $html;
				}else{
					echo '<span class="tools-alert tools-alert-red">'.Panel::lang('No photos yet').'</span>';
				}
			?>

		</div>
	</div>

