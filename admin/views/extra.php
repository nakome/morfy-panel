<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>

	<ul class="breadcrumbs">
	  <li><a href="#"><i class="ti-home"></i></a></li>
	  <li class="unavailable"><a href="#"><?php echo Panel::lang('Settings');?></a></li>
	  <li class="current"><a href="#">Extra</a></li>
	</ul>

  <div class="row">
    <div class="box-1 col">
      <div class="tools-alert"><b>Archive: </b>/library/extra-function-php</div>
    </div>
  </div>


	<div class="row">
		<div class="box-1 col">
      <h3>Analytics</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('analytics', array('6546545325'));</pre>
      <hr>
      <h3>Show Snippets</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('snippet', array('snippet name'));</pre>
      <hr>
      <h3>Search in folder empty = blog</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('search',array('name of folder'));</pre>
      <hr>
      <h3>Sitemap</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('sitemap');</pre>
      <hr>
      <h3>Discus Comments</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('discus', array('minickname'));</pre>
      <hr>
      <h3>Facebook Comments</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('fbcomments', array('url','num of comments'));</pre>
      <hr>
      <h3>Show Pages like blog</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('pages',array('folder name','num of posts')); </pre>
      <hr>
      <h3>Show Single Portfolio by id</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('portfolio', array('folder id here',false));</pre>
      <hr>
      <h3>Show All fotos of id</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('portfolio', array('folder id here',true));</pre>
      <hr>
      <h3>Contact</h3>
      <pre data-codetype="Php Code:">Morfy::factory()->runAction('contact',array('yourmail@gmail.com'));</pre>
      <hr>
      <h3>Make Custom Codes in library/extra-functions.php</h3>
      <pre data-codetype="Php Code:">

Simple function

/* php} Morfy::factory()->runAction('custom', array('Jhon')); {/php}
----------------------------------------------------------------------------------/
Morfy::factory()->addAction('custom', function($name) {
$html = '&lt;p&gt; '.$name.' &lt;/p&gt;'
echo $name;
});
      </pre>
      <hr>

      <pre data-codetype="Php Code:">
filter function
/* - {custom}
------------------*/
Morfy::factory()->addFilter('content', function($content){
$code = 'Hello World';
return str_replace('{custom}', $code, $content);
});
        </pre>
        <hr>
    </div>
	</div>

