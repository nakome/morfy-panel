<?php defined('PANEL_ACCESS') or die('No direct script access.');?>


<form  method="POST" action="?action=login">
  <input type="hidden" name="token" value="<?php echo  Panel::factory()->generateToken(); ?>">
  <div class="row">
   <div class="box-3 col"></div>
   <div class="box-3 col">
     <label>Login</label>
     <input class="pull-left" type="password" name="password" id="password" placeholder="<?php echo Panel::lang('Your password');?>">
     <input class="pull-left" type="submit" class="btn" value="<?php echo Panel::Lang('Login') ?>">
   </div>
   <div class="box-3 col"></div>
  </div>
</form>

