<?php defined('PANEL_ACCESS') or die('No direct script access.');
    $dir = Panel::factory()->sanitizeURL(Panel::Request_get('f'));
?>

<ul class="breadcrumbs">
  <li><a href="#"><i class="ti-home"></i></a></li>
  <li class="unavailable"><a href="#"><?php echo Panel::lang('Pages');?></a></li>
  <li class="current"><a href="#"><?php echo Panel::lang('New File');?></a></li>
</ul>

<form class="forms formFile" method="POST">
	<input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">

	<?php if($type == 'css'){ ?>
		<input type="hidden" name="stylesheets" value="stylesheets">
	<?php }elseif($type == 'js'){ ?>
		<input type="hidden" name="javascript" value="javascript">
	<?php }elseif($type == 'markdown'){ ?>
		<?php if($dir){ ?>
		<input type="hidden" name="pages" value="pages">
		<input type="hidden" name="haveDir" value="<?php echo $dir;?>">
		<?php }else{ ?>
		<input type="hidden" name="pages" value="pages">
		<?php }; ?>
	<?php }elseif($type == 'php'){ ?>
		<input type="hidden" name="templates" value="templates">
	<?php } ?>


	<div class="row">
		<div class="box-1 col">
			<label class="pull-left"><?php echo Panel::lang('Filename');?></label>
				<input class="pull-left" type="text" name="filename" placeholder="<?php echo Panel::lang('Name of file here');?>" required />
		</div>
	</div>

	<div class="row">
		<div  class="box-1 col">
		<?php if($type == 'markdown'){ ?>
			<textarea name="content" class="editor-area" id="editor-area">
<?php if($type == 'markdown'){
echo 'Title: Hello
Description: Hello world
Keywords: hello,world
Author: Morfy
Date: '.Date('d/m/Y').'
Robots:
Tags:hello,world
Template:index

----

## Welcome to Morfy
';};?></textarea>
			<?php }else{ ?>
				<div class="editor">
				    <div class="editor-control" id="editor-control"></div>
				    <noscript class="noscript" class="hide"></noscript>
				    <textarea name="content" class="editor-area" id="editor-area" placeholder="Start here.."></textarea>
				</div>
			<?php }; ?>


			<a href="<?php echo Panel::Site_url(); ?>"  class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
			<input type="submit" name="saveFile" class="btn" value="<?php echo Panel::Lang('Save File'); ?>">
		</div>
	</div>
</form>