<?php
	defined('PANEL_ACCESS') or die('No direct script access.');
	$dir = Panel::Request_get('g');
?>

	<ul class="breadcrumbs">
		<li><a href="#"><i class="ti-home"></i></a></li>
		<li class="unavailable"><a href="#"><?php echo Panel::lang('Content');?></a></li>
		<li class="current"><a href="#"><?php echo ucfirst($dir);?></a></li>
		<span class="search">
			<form method="post" actiom="" id="search">
				 <input type="search" name="name">
				 <input type="submit" name="submit" class="hide">
			</form>
		</span>
	</ul>




<?php 
	Morfy::factory()->runAction('backendSearch');
	Morfy::factory()->runAction('dp');
	return Panel::ReadFolder('../content/'.$dir,'md');
?>