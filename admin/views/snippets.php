<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>

<ul class="breadcrumbs">
  <li><a href="#"><i class="ti-home"></i></a></li>
  <li class="unavailable"><a href="#"><?php echo Panel::lang('Snippets');?></a></li>
  <li class="current"><a href="#"><?php echo Panel::lang('View Snippets');?></a></li>
</ul>
<div class="row">
	<div class="box-2 col">
	    <pre data-codetype="Php Code:">{php} Morfy::factory()->runAction('snippet',array('header')); {/php}</pre>
	</div>
</div>
<?php Morfy::factory()->runAction('snippets'); ?>