<?php 
  defined('PANEL_ACCESS') or die('No direct script access.'); 
  Morfy::factory()->runAction('createSnippets');
?>

<ul class="breadcrumbs">
  <li><a href="#"><i class="ti-home"></i></a></li>
  <li class="unavailable"><a href="#"><?php echo Panel::lang('Snippets');?></a></li>
  <li class="current"><a href="#"><?php echo Panel::lang('Create Snippet');?></a></li>
</ul>


<form class="forms formFile" method="POST">
  <input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">

  <div class="row">
    <div class="box-1 col">
      <label class="pull-left"><?php echo Panel::lang('Filename');?></label>
        <input class="pull-left" type="text" name="filename" placeholder="<?php echo Panel::lang('Name of file here');?>" required />
    </div>
  </div>

  <div class="row">
    <div  class="box-1 col">
        <div class="editor">
            <div class="editor-control" id="editor-control"></div>
            <noscript class="noscript" class="hide"></noscript>
            <textarea name="content" class="editor-area" id="editor-area" placeholder="Start here.."></textarea>
        </div>
      <a href="<?php echo Panel::Site_url(); ?>?g=snippets"  class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
      <input type="submit" name="saveSnippet" class="btn" value="<?php echo Panel::Lang('Save File'); ?>">
    </div>
  </div>
</form>