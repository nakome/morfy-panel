<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>


<ul class="breadcrumbs">
  <li><a href="#"><i class="ti-home"></i></a></li>
  <li class="unavailable"><a href="#"><?php echo Panel::lang('Theme');?></a></li>
  <li class="current"><a href="#"><?php echo Panel::lang('Templates');?></a></li>
</ul>

<?php Morfy::factory()->runAction('Templates'); ?>