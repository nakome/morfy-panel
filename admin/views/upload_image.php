<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>

<?php Panel::uploadImages(); ?>

	<div class="row">
		<div class="box-1 col">
			<ul class="breadcrumbs">
			  <li><a href="#"><i class="ti-home"></i></a></li>
			  <li class="unavailable"><a href="#"><?php echo Panel::lang('Images');?></a></li>
			  <li class="current"><a href="#"><?php echo Panel::lang('All Images');?></a></li>
			</ul>
		</div>
	</div>

	<div id="progress">
	    <div class="spinner" role="spinner">
	        <div class="spinner-icon"></div>
	    </div>
	</div>

	<div class="row">
		<form class="formFile" method="post"  enctype="multipart/form-data">
			<div class="box-2 col">
				<label><?php echo Panel::lang('Choose a Image file');?>:  <small>*</small>
					<input type="file"  class="upload" name="file_upload" id="image-input" accept="image/x-png, image/gif, image/jpeg"  />
				</label>

				<label><?php echo Panel::lang('With');?>: <small>*</small>
					<input type="text"  required  name="width" id="width" value="250">
				</label>

				<label><?php echo Panel::lang('Height');?>:  <small>*</small>
					<input type="text" required name="height" id="height" value="180">
				</label>

				<label><?php echo Panel::lang('Title');?>:  <small>*</small>
					<input type="text"  required  name="title" id="title">
				</label>

				<label><?php echo Panel::lang('Description');?>:  <small>*</small>
					<textarea name="description" id="description" rows="3" required></textarea>
				</label>

				<label><?php echo Panel::lang('Link');?>:  <small><?php echo Panel::lang('Only one for Image');?> *</small>
					<input type="text"  required  name="link" id="link">
				</label>

				<a href="<?php echo Panel::Site_url();?>?g=images"  class="btn btn-danger"><?php echo Panel::Lang('Cancel'); ?></a>
				<input type="submit" name="upload" id="upload" class="btn" value="<?php echo Panel::lang('Upload');?>">
			</div>
		</form>
		<div  class="box-2 col">
			<img  id="image-display" src="<?php echo Panel::Site_url().'/assets/img/nopreview.jpg'; ?>" alt="">
		</div>
	</div>

