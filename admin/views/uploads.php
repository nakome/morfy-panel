<?php defined('PANEL_ACCESS') or die('No direct script access.'); ?>

  <ul class="breadcrumbs">
    <li><a href="#"><i class="ti-home"></i></a></li>
    <li class="unavailable"><a href="#"><?php echo Panel::lang('Uploads');?></a></li>
    <li class="current"><a href="#"><?php echo Panel::lang('View Files');?></a></li>
  </ul>
    

    <div id="progress">
        <div class="spinner" role="spinner">
            <div class="spinner-icon"></div>
        </div>
    </div>

    <form class="formFile" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="token" value="<?php echo Panel::factory()->generateToken(); ?>">
        <input type="text" id="uploadFile" class="fake" placeholder="Choose File" disabled="disabled" />
        <div class="fileUpload btn">
          <span>Select File</span>
          <input name="filesToUpload[]" id="filesToUpload" class="upload" type="file"/>
        </div>
        <input type="submit" name="uploadFile" value="<?php echo Panel::lang('Upload')?>">
    </form>

<?php Morfy::factory()->runAction('uploadFiles'); ?>