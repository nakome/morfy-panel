<?php


/* - Morfy::factory()->runAction('pages',array('name'=>'blog' , 'num' => 2));
--------------------------------------------------------------------------------*/
Morfy::factory()->addAction('pages', function($name,$num) {
  // All pages
    $posts = Morfy::factory()->getPages(CONTENT_PATH . '/'.$name.'/', 'date', 'ASCD', array('404','index'));
    // Limit of pages
    $limit = $num;
    //intialize a new array of files that we want to show
    $blogPosts = array();
    //add a file to the $goodfiles array if its name doesn't begin with a period
    foreach($posts as $f){
        // Insert one or more elements in array
        array_push($blogPosts,$f);
    }
    // Divide an array into fragments
    $pages = array_chunk($blogPosts, $limit);
    // Get page
    $pgkey = isset($_GET['page']) ? $_GET['page'] : 0;
    $pages[$pgkey];
    // get template
    echo '<div class="row"><div class="box-1 col">';
    foreach($pages[$pgkey] as $post){
        // vars if you like you can add more here
        $site_url   = Morfy::$config['site_url'];
        $slug       = $post['slug'];
        $title      = $post['title'];
        $date       = $post['date'];
        $author     = ($post['author']) ? $post['author'] : 'No Author';
        $tags       = ($post['tags']) ? $post['tags'] : 'No Tags';
        $content    = $post['content_short'];
        $thumbnail  = ($post['thumbnail']) ? $post['thumbnail'] : Morfy::$config['site_url'].'/themes/clarity/assets/img/image-demo.jpg';

        // you can customize this if you like
        $html = '<h3><a href="'.Morfy::$config['site_url'].'/blog/'.$slug.'" title="'.$title.'">'.$title.'</a></h3>'.$content;
        echo $html;
    }
    echo '</div></div>';


    // If empty active first
    if( empty($_GET['page']) ) $page = 1;
    else $page = isset($_GET['page']) ? $_GET['page'] : 0;
    $total = count($pages);
    // Get page
    $p = $_GET['page'];
    $pagination = '<ul class="list-inline pull-right">';
    $pagination .=  $p > 0 ? '<li><a  class="btn btn-default" href="?page='.($p -1).'">New Posts &nbsp;<i class="fa fa-arrow-right"></i></a></li>':'';
    $pagination .= ($p+1) < $total ? '<li><a  class="btn btn-default" href="?page='.($p+1).'">Old Posts &nbsp;<i class="fa fa-arrow-right"></i></a></li>':'';
    $pagination .= '</ul>';
    echo $pagination;
});




/* {php} Morfy::factory()->runAction('analytics', array('id' => '')); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('analytics', function($id) {
    $TrackingId = $id;
    // javascript
    $javascript = "<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '$TrackingId', 'auto');
ga('send', 'pageview');
</script>";
    // render javascript
    echo $javascript;
});



/* Morfy::factory()->runAction('share', array('facebook' => '','twitter'=>'','github'=>''));
----------------------------------------------------------------------------------------*/
Morfy::factory()->addAction('share', function($facebook,$twitter,$github) {
    $html = '
      <ul class="list-inline text-center">
        <li><a href="http://twitter.com/'.$facebook.'">Twitter</a></li>
        <li><a href="http://facebook.com/'.$twitter.'">facebook</a></li>
        <li><a href="http://github.com/'.$github.'">Github</a></li>
      </ul>';
    echo $html;
});




/* {php} Morfy::factory()->runAction('discus', array('name' => 'monchovarela')); {/php}
-----------------------------------------------------------------------------------------*/
Morfy::factory()->addAction('discus', function($name) {
    $html = '<div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$name.'";
        (function() {
        var dsq = document.createElement("script");
        dsq.type = "text/javascript";
        dsq.async = true;
        dsq.src = "//" + disqus_shortname + ".disqus.com/embed.js";
        (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(dsq);
        })();
    </script>';
    echo $html;
});




/**
*  Facebook Comments
*  @author Dextra
* {php} Morfy::factory()->runAction('fbComments', array('fburlsite' => 'monchovarela','fblimit' => '')); {/php}
*  -----------------------------------------------------------------------------------------*/
Morfy::factory()->addAction('fbComments', function($fburlsite,$fblimit = 5) {
  $fburlsite = ($fburlsite) ? $fburlsite : Morfy::$config['site_url'];
  $html = '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
  <div class="fb-comments" data-href="'.$fburlsite.'" data-width="100%" data-numposts="'.$fblimit.'"></div>';
  echo $html;
});








/* {php} Morfy::factory()->runAction('portfolio',array(id,all)); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('portfolio', function($id = '',$all = false) {

  // get json
  $file = file_get_contents('public/photos.json');
  // decode
  $json = json_decode($file,true);
  $num = 0;
  // if id show
  if($id){
      // item id
      $item = $json[$id];
      // custom varsi not use all for this demo
      $url = Morfy::$config['site_url'].'/public/images/';
      $title = $item['title'];
      $description = $item['description'];
      $link = $item['link'];
      $id = $item['id'];
      $width = $item['width'];
      $height = $item['height'];
      $image = $url.$item['image'];
      $directory = 'public/images/'.$id.'/';
      $images = glob($directory . "*");
      $num = 1;

      if($all){

        $html = '
        <div class="row">
          <div class="col-md-12">
            <h3>'.$title.'</h3>
            '.html_entity_decode($description).'
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <figure class="item">
              <img src="'.$image.'" alt="'.$title.'">
            </figure>';
          if(count($images) > 0){
            foreach ($images as $th) {
                $html .= '<figure> <img src="'.Morfy::$config['site_url'].'/'.$th.'" alt="'.$title.'"></figure>';
            }
          }

        $html .= '</div></div>';

        echo $html;
      }else{
        $html = '
          <div class="row">
              <div class="col-md-6">
                 <img class="img-responsive" src="'.$image.'" alt="'.$title.'">
              </div>
              <div class="col-md-6">
                  <div class="well">
                    <h3>'.$title.'</h3>
                    '.html_entity_decode($description).'
                    <hr>
                    <a class="btn btn-default" href="'.$link.'">Get more..</a>
                  </div>
              </div>
          </div>';
          echo $html;
      }
  }else{
    echo '<div class="well">You need to provide the id</div>';
  }


});




/* {php} Morfy::factory()->runAction('sitemap'); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('sitemap', function() {

  $a = array_filter(glob('content/*'),'is_file');
  $a = str_replace('content/', '', $a);
  $a = str_replace('.md', '', $a);

  $html = '<ul class="tree">';

  foreach ($a as $one) {
    if($one != 'index' && $one != '404'){
      $html .= '<li><a href="'.$one.'">'.$one.'</a></li>';
    }
  }

  $b = array_filter(glob('content/*'),'is_dir');
  $b = str_replace('content/', '', $b);

  foreach ($b as $dir) {
    $c = array_filter(glob('content/'.$dir.'/*'),'is_file');
    $c = str_replace('content/'.$dir.'/', '', $c);
    $c = str_replace('.md', '', $c);
    $html .=  '<li><b>'.$dir.'</b><ul class="three">';
    foreach ($c as $second) {
      if($second != 'index'){
        $html .=  '<li><a href="'.$dir.'/'.$second.'">'.$second.'</a></li>';
      }
    }
    $html .= '</ul></li>';
  }
  $html .= '</ul>';
  echo $html;
});




/* {php} Morfy::factory()->runAction('search',array('blog')); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('search', function($name = 'blog') {

    $html = '
    <div class="row">
      <div class="col-md-12">
        <form method="post" actiom="" class="form-inline">
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Search by title" required>
            </div>
            <input type="submit" name="Search" class="btn btn-default" value="Search">
        </form><br>';
    $pages = Morfy::factory()->getPages(CONTENT_PATH.'/'.$name.'/', 'date', 'DESC', array('404','index'));
    if(isset($_POST['Search'])){
        $name = strtolower($_POST['name']);
        $counter = 0;
        $html .= '<div class="well">
                    <h4>Results for <b>' .$_POST['name']. '</b> : </h4>
                  <ol>';
        foreach($pages as $page){
            $title = strtolower($page['title']);
            if (strpos($title , $name) !== false)  {
                $counter++;
                $html .= '<li><a href="blog/'.$page['slug'].'">' .$page['title']. '</a></li>';
            }
         }
        // no matches found
        if ($counter === 0) {
            $html .= '<p><i>no matches found</i></p>';
        }
        $html .= '</ol>';
    }
    $html .= '</div></div>';
    echo $html;
});




/* {php} Morfy::factory()->runAction('contact',array('yourmail@gmail.com')); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('contact', function($mail = ' ') {

  if(isset($_POST['Submit'])){
      // vars
      $error = '';
      $recepient = $mail;
      $sitename = Morfy::$config['site_url'];
      $service = trim($_POST["subject"]);
      $name = trim($_POST["name"]);
      $email = trim($_POST["email"]);
      $tel = trim($_POST["tel"]);
      $text = trim($_POST["message"]);
      $message = "Service: $service \n\nName: $name \nPhone: $tel \n\nMessage: $text";
      $pagetitle = "New message form \"$sitename\"";
      // send mail
      if(mail(
        $recepient,
        $pagetitle,
        $message,
        "Content-type: text/plain; charset=\"utf-8\" \nFrom: $name <$email>"
      )){
        // success
        $error = '<span class="alert alert-success">Thanks, Your message has been send ....</span>';
      }else{
        // error
        $error = '<span class="alert alert-danger">OOpss houston have a problem refresh and try..</span>';
      };
  }

    // append row and col
    $html = '<div class="row"><div class="col-md-6">';
    // show error
    echo $error;
    // template bootstrap
    $html .= '
                <div class="well">
                  <form class="form" action="" method="post"  name="form1">
                  <div class="form-group">
                    <input type="text" name="name" class="form-control" required placeholder="Your Name">
                  </div>
                  <div class="form-group">
                    <input type="text" name="tel" class="form-control" required placeholder="Your Telephone">
                  </div>
                  <div class="form-group">
                    <input type="email" name="email" class="form-control" required placeholder="Your Email">
                  </div>
                  <div class="form-group">
                    <input type="text" name="subject" class="form-control" required  placeholder="Your Subject">
                  </div>
                  <div class="form-group">
                    <textarea  name="message" class="form-control" rows="8" required placeholder="Your Message"></textarea>
                  </div>
                  <input type="submit" name="Submit" class="btn" value="Submit"> 
              </div>
            </div><!-- col -->
        </div><!-- row -->
    ';
    echo $html;
});


/* {php} Morfy::factory()->runAction('snippet',array('header')); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('snippet', function($file) {
   if(file_exists('snippets/'.$file.'.php')){
      return include 'snippets/'.$file.'.php';
   }
});

/* {php} Morfy::factory()->runAction('breadcrumbs'); {/php}
-----------------------------------------------------------------------------*/
Morfy::factory()->addAction('breadcrumbs', function() {
    $paths = Morfy::factory()->getUriSegments();
    $total_paths = count($paths);
    // Path lifter
    $lift = "";
    // Breadcrumb's HTML markup
    $html = '<ul>';
    for($i = 0; $i < $total_paths; $i++) {
        $lift .= '/' . $paths[$i];
        $data = Morfy::factory()->getPage(file_exists(CONTENT_PATH . '/' . $lift . '/index.md') || file_exists(CONTENT_PATH . '/' . $lift . '.md') ? $lift : '404');
        if($i < $total_paths - 1) {
            $html .= '<li><a href="' . Morfy::$config['site_url'] . $lift . '">' . $data['title'] . '</a></li>';
        } else {
            $html .= '<li><span>' . $data['title'] . '</span></li>';
        }
    }
    $html .= '</ul>';
    echo $html;
});



/* FILTERS
-------------------------------*/



// {share}
Morfy::factory()->addFilter('content', function($content){
    $code = '<ul>
        <li><a href="http://facebook.com/nakome">Facebook</a></li>
        <li><a href="http://twitter.com/nakome">Twitter</a></li>
        <li><a href="http://youtube.com/nakome">Youtube</a></li>
      </ul>';
    return str_replace('{share}', $code, $content);
});